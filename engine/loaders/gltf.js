import merge from "../../utils/merge";
import {
  SkinnedMesh,
  AnimationMixer,
  AnimationClip,
  MeshStandardMaterial,
  Group,
  Mesh,
  Bone,
} from "three";
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';

let i = 0;

function traverse(obj) {
  // if (obj.name == "armature") {
  //   this._avatar = obj;
  // }
  if (obj instanceof Array) {
    for (let item of obj) {
      traverse.bind(this)(item);
    }
  } else {
    console.log(obj.name, obj);
    if (obj instanceof AnimationClip) {
      this.animations[obj.name + "_" + (i++)] = obj;
    } else if (obj instanceof SkinnedMesh) {
      let name = obj.name.toLowerCase();
      this.models[name] = obj.geometry;
      this.materials[name] = obj.material;
      this.skeletons[name] = obj.skeleton;
      if (! this._avatar) {
        this._avatar = obj;
        obj.add(obj.skeleton.bones[0])
      }

      if (Array.isArray(obj.material)) {
        for (let sub of obj.material) {
          sub.skinning = true;
          this.materials[sub.name.toLowerCase()] = sub;
        }
      } else {
        this.materials[obj.material.name] = obj.material;
      }
    } else if (obj.children) {
      for (let child of obj.children) {
        traverse.bind(this)(child);
      }
    }
  }
}

export default function import_gltf(data) {
  let game = this;
  i = 0;

  return new Promise((resolve, reject) => {
    var loader = new GLTFLoader();
    // loader.options.convertUpAxis = true;
    let source = this.resolveAsset(`models/${data.name}.gltf`);

    console.log("source", source);
    loader.load(`${source}?dt=${Date.now()}`, (obj) => {

      console.log("gltf import", obj);

      traverse.bind(this)(obj.scene);
      traverse.bind(this)(obj.animations);

      resolve();
    }, (p) => {
      // console.log("progress", p)
    }, (ex) => {
      // console.log("error", ex);
    });
  });
}
