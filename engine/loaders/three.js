import merge from "../../utils/merge";
import {SkinnedMesh, AnimationMixer, AnimationClip, ObjectLoader} from "three";



function traverse(obj) {
  if (obj instanceof SkinnedMesh) {
    this.models[obj.name.toLowerCase()] = obj.geometry;
    this.materials[obj.name.toLowerCase()] = obj.material;
    if (Array.isArray(obj.material)) {
      for (let sub of obj.material) {
        sub.skinning = true;
        this.materials[sub.name.toLowerCase()] = sub;
      }
    }
  } else {
    for (let child of obj.children) {
      traverse.bind(this)(child);
    }
  }
}

export default function three_loader (data) {

  let entry = merge({
    exclude: ["Lamp", "Camera"]
  }, data);

  let game = this;

  return new Promise((resolve, reject) => {
    var loader = new ObjectLoader();
    // loader.options.convertUpAxis = true;
    let source = this.resolveAsset(`static/models/${data.name}.json`);

    loader.load(`${source}?dt=${Date.now()}`, (obj) => {
    // let obj = loader.parseObject(source);

      console.log("three import", obj);

      traverse.bind(this)(obj);
      // let name = data.override_name || data.name;
      // game.models[name] = obj.children[0].geometry;
      // game.materials[name] = obj.children[0].material;

      // for (let child of obj.children) {
      //   if (entry.exclude.indexOf(child.name) === -1) {
      //     let name = child.name.toLowerCase();
      //     game.models[name] = child.geometry;
      //     game.materials[name] = child.material;
      //     if (Array.isArray(child.material)) {
      //       for (let sub of child.material) {
      //         game.materials[sub.name.toLowerCase()] = sub;
      //       }
      //     }
      //   }
      // }

      resolve();
    });
  });
}
