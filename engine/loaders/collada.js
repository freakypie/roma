// import ColladaLoader from "three-collada-loader";
import { ColladaLoader } from 'three/examples/jsm/loaders/ColladaLoader.js';
import {SkinnedMesh, AnimationClip} from "three";

import merge from "../../utils/merge";

function traverse(obj) {
  if (obj instanceof Array) {
    for (let item of obj) {
      traverse.bind(this)(item);
    }
  } else {
    console.log(obj.name, obj);
    if (obj instanceof AnimationClip) {
      this.animations[obj.name] = obj;
    } else if (obj instanceof SkinnedMesh) {
      let name = obj.name.toLowerCase();
      this.models[name] = obj.geometry;
      this.materials[name] = obj.material;
      this.skeletons[name] = obj.skeleton;
      this._avatar = obj;
      if (Array.isArray(obj.material)) {
        for (let sub of obj.material) {
          sub.skinning = true;
          this.materials[sub.name.toLowerCase()] = sub;
        }
      } else {
        this.materials[obj.material.name] = obj.material;
      }
    } else if (obj.children) {
      for (let child of obj.children) {
        traverse.bind(this)(child);
      }
    }
  }
}

export default function collada (data) {

  let entry = merge({
    exclude: ["Lamp", "Camera"]
  }, data);

  let game = this;

  return new Promise((resolve, reject) => {
    var loader = new ColladaLoader();
    // loader.options.convertUpAxis = true;
    let source = this.resolveAsset(entry.src || `static/models/${entry.name}.dae`);
    console.log("collada source", source);
    loader.load(`${source}?dt=${Date.now()}`, (collada) => {

      console.log("collada imported", collada);

      traverse.bind(this)(collada.scene);
      traverse.bind(this)(collada.animations);

      // for (let obj of collada.scene.children) {
      //   if (entry.exclude.indexOf(obj.name) === -1) {
      //     let mesh = obj.children[0];
      //     let name = obj.name.toLowerCase();
      //     if (entry.override_name) {
      //       name = entry.override_name;
      //     }

      //     if (mesh.geometry) {
      //       game.models[name] = mesh.geometry;
      //     }
      // //     game.meshes[name] = mesh;
      //   }
      // }

      resolve();
    });
  });
}
