/* global window */
import {
  /*Font, */ LinearFilter,
  OrthographicCamera,
  PerspectiveCamera,
  Texture,
  TextureLoader,
  Vector3,
} from "three";
import Mod from "../modules/base.js";
import merge from "../utils/merge";
import bvh from "./loaders/bvh";
import collada from "./loaders/collada";
import gltf from "./loaders/gltf";
import textmap from "./loaders/textmap";
import three from "./loaders/three";
import Sprite from "./sprite";

import { OverrideMaterialManager } from "postprocessing";
import { TextureData, type GameObjectDef, type SpriteDef } from "./defs";

OverrideMaterialManager.workaroundEnabled = true;

// @ts-ignore
// import modules from "../modules/**/*.js";
// @ts-ignore
// import behaviors from "../behaviors/**/*.js";

export default class Game extends Sprite {
  name: string;
  _time: number = 0;
  _pausedTime: number = 0;
  _paused?: number;
  last: number;
  loops: number;
  sprites: Sprite[];
  textures: Record<string, Texture>;
  blueprints: Record<string, Record<string, any>>;
  materials: Record<string, () => any>;
  stage: Record<string, Sprite> = {};
  options: Record<string, any>;
  log: boolean = false;
  interface: any = {};
  renderer?: any;
  loaders: Record<string, (data: GameObjectDef) => Promise<any>>;
  camera: PerspectiveCamera | OrthographicCamera | undefined;
  cameraWidth: number = 4;

  get time() {
    return this._time - this._pausedTime;
  }
  set time(v) {
    this._time = v;
  }

  get paused() {
    return this._paused !== undefined;
  }
  set paused(v) {
    if (v) {
      this._paused = Date.now();
    } else {
      this._pausedTime += Date.now() - (this._pausedTime || 0);
      this._paused = undefined;
    }
  }

  constructor(
    options: Record<string, any>,
    callback?: Function,
    extraModules = {},
  ) {
    // @ts-ignore
    super(null);
    this.name = options.name || "game";
    this.options = options;
    this.assets = options.assets;
    this.blueprints = {};
    this.animations = {};
    this.fonts = {};
    this.materials = {};
    this.models = {};
    this.skeletons = {};
    this.textures = {};
    this.sprites = [];
    this.resources = [];
    this.timeScale = 1000 / 30;
    this.timeScaleSquared = Math.pow(this.timeScale, 2);
    this.callback = callback;
    this.listeners = [];

    // time
    this._time = Date.now();
    this._pausedTime = Date.now();

    // fps
    this.loops = 0;
    this.last = Date.now();

    let game = this;

    if (extraModules) {
      for (let key in extraModules) {
        let mod = extraModules[key];
        Mod.modules[key] = mod.default ? mod.default : mod;
      }
    }

    // this.sprite_behaviors = behaviors;
    this.loaders = {
      async sprite(data: SpriteDef) {
        let sprite = await Sprite.load(game, data);
        game.sprites.push(sprite);
        await sprite.postLoad();
        return sprite;
      },
      async module(data) {
        // TODO: let mod = (await import(/* webpackMode: "lazy-once" */ "../modules/" + data.name)).default;
        let mod = this.resolveModule(data.name);
        game.behaviors.push(new mod(game, data));
        return mod;
      },
      // async json(data) {
      //   let mod = this.resolveAsset(data.name);
      //   await game.load(mod);
      //   return mod;
      // },
      async json(mod) {
        // TODO: let mod = (await import(/* webpackMode: "lazy-once" */ "assets/" + data.name));
        await game.load(mod);
        return mod;
      },
      async fetch(mod) {
        // TODO: let mod = (await import(/* webpackMode: "lazy-once" */ "assets/" + data.name));
        console.log("fetching", JSON.stringify(mod, null, 4));
        return game.load(await mod.function());
      },
      // async material(data) {
      //   return new ({

      //   })
      // },
      async material(data) {},
      async texture(data: TextureData) {
        return await new Promise((resolve, reject) => {
          let url = data.src;

          if (data.gradient) {
            const size = data.gradient.size || 256;
            const canvas = document.createElement("canvas");
            canvas.width = size;
            canvas.height = size;
            const context = canvas.getContext("2d");
            context.rect(0, 0, size, size);
            const direction = data.gradient.direction || [
              size / 2,
              0,
              size / 2,
              size,
            ];
            const gradient = context.createLinearGradient(...direction);
            const stops = data.gradient.stops;
            stops.forEach((stop, i) =>
              gradient.addColorStop(i / (stops.length - 1), stop),
            );
            context.fillStyle = gradient;
            context.fill();

            let loader = new TextureLoader().load(
              canvas.toDataURL(),
              function (texture) {
                texture.minFilter = LinearFilter;
                game.textures[data.name] = texture;
                resolve(texture);
              },
              function (xhr) {},
              function (e) {
                console.error(
                  "Error while loading texture",
                  url,
                  e,
                  loader,
                  arguments,
                );
              },
            );
          } else if (url instanceof Image) {
            let texture = new Texture();
            texture.minFilter = LinearFilter;
            texture.image = url;
            texture.needsUpdate = true;
            game.textures[data.name] = texture;
            resolve(texture);
          } else if (/^blob:/.test(url)) {
            let image = new Image();
            image.onload = () => {
              let texture = new Texture();
              texture.minFilter = data.minFilter || LinearFilter;
              texture.image = image;
              texture.needsUpdate = true;
              game.textures[data.name] = texture;
              resolve(texture);
            };
            image.onerror = (e) => {
              console.log(e);
            };
            image.src = url;
          } else if (url) {
            if (!/^blob:|^data:image|\?/.test(url)) {
              url = this.resolveAsset(url) + "?dt=" + Date.now();
            }
            let loader = new TextureLoader().load(
              url,
              function (texture: Texture) {
                texture.minFilter = data.minFilter || LinearFilter;
                texture.magFilter = data.magFilter || LinearFilter;
                texture.anisotropy = data.anistropy || 1;
                texture.generateMipmaps = data.generateMipMaps || false;
                // texture.wrapS = THREE.RepeatWrapping;
                // texture.wrapT = THREE.RepeatWrapping;
                // texture.offset.x = 90 / (2*Math.PI);
                game.textures[data.name] = texture;
                resolve(texture);
              },
              function (xhr) {
                console.log((xhr.loaded / xhr.total) * 100 + "% loaded");
              },
              function (e) {
                console.error(
                  "Error while loading texture image",
                  url,
                  e,
                  loader,
                  arguments,
                );
              },
            );
          }
        });
      },
      // async font(data) {
      //   let fontjson = Mod.modules[data.name];
      //   game.fonts[data.name] = new Font(fontjson);
      // },
      async loader(data) {
        if (!behaviors[data.name]) {
          console.warn("missing", data.name);
        }
        // game.loaders[data.name] = await this.sprite_behaviors[data.name]();
        // TODO: game.loaders[data.name] = (await import(/* webpackMode: "eager" */ `behaviors/${data.name}`)).default.bind(game);
      },
      async resource(data) {
        game.resources[data.name] = data;
      },
      collada,
      three,
      gltf,
      textmap,
      bvh,
    };
    if (import.meta.hot) {
      window._hot_game = this;
    }
  }

  resolveModule(name) {
    if (!Mod.modules[name]) {
      console.error(
        `missing module ***${name}***
Be sure to import the module before Game. Perhaps:

  "import 'roma/modules/${name}.js'; "

  OR

  "import 'roma/behaviors/${name}.js'; "
`,
        Object.keys(Mod.modules),
      );
    }
    return Mod.modules[name];
  }

  resolveAsset(name: string) {
    return name;
    // return this.resolveModule(name, this.assets);
  }

  getBlueprintData(name: string) {
    return this.resolveData({
      extends: name,
    });
  }

  resolveData(data: Record<string, any>) {
    if (data.extends) {
      let blueprint = this.blueprints[data.extends];
      if (!blueprint) {
        console.error(
          `no blueprint with '${data.extends}' // ['${Object.keys(
            this.blueprints,
          ).join("', '")}']`,
        );
        console.log("blueprints", this.blueprints);
      }
      data = merge(this.blueprints[data.extends], data);
    }
    return data;
  }

  async load(data: GameObjectDef[], log = false) {
    await Mod.ready();
    if (log) {
      this.log = false;
    } else {
      log = this.log;
    }

    if (log) console.log("loading", this.name);
    let retval = [];
    this.data = data;

    // load components
    if (data.default) {
      data = data.default;
    }
    let components = data.components;
    if (!components) {
      components = data;
    }
    for (let c of components.filter((c) => !c.ignore)) {
      c = this.resolveData(c);
      if (c.blueprint) {
        c = merge({}, c);
        let name = c.blueprint;
        delete c.blueprint;
        c.name = name;
        this.blueprints[name] = c;
        if (log) console.log("blueprint", name);
      } else {
        let loader = this.loaders[c.type];
        if (loader) {
          if (log) console.log("loading", c.type);
          retval.push(await loader.bind(this)(c));
        } else {
          console.error("can't load", c);
        }
      }
    }

    return retval;
  }

  stop() {
    if (this._start || this._render) {
      this.paused = true;
      console.warn("game stopped", this._start, this._render);
      clearInterval(this._start);
      window.cancelAnimationFrame(this._render);
      this._start = null;
      this._render = null;
    }
  }

  start() {
    this.stop();
    this.paused = false;
    this._start = setInterval(this.loop.bind(this), this.timeScale);
    this._render = window.requestAnimationFrame(this.render.bind(this));
    console.warn("game started", this.timeScale, this._start, this._render);
  }

  async loop() {
    if (!this._looping) {
      this._looping = true;

      // get lps
      this.time = Date.now();
      this.loops += 1;
      if (this.time - this.last > 1000) {
        this.fps = (this.loops / (this.time - this.last)) * 1000;
        this.last = this.time;
        this.loops = 0;
        console.log(`lps: ${this.fps}`);
      }

      for (let behavior of this.behaviors) {
        if (behavior.preUpdate) {
          await behavior.preUpdate();
        }
      }
      this.update();

      let remove = [];
      for (let sprite of this.sprites) {
        if (sprite.update(this.time) === false) {
          remove.push(sprite);
        }
      }

      for (let r of remove) {
        r.destroy();
        let idx = this.sprites.indexOf(r);
        if (idx > -1) this.sprites.splice(idx, 1);
      }

      if (this.callback) this.callback();

      this._looping = false;
      // } else {
      //   console.warn("skipping frame");
    }
  }

  render() {
    this.time = Date.now();
    this.draw();
    window.cancelAnimationFrame(this._render);
    this._render = window.requestAnimationFrame(this.render.bind(this));
  }

  clear() {
    for (let behavior of this.behaviors) {
      behavior.destroy();
    }
    for (let sprite of this.sprites.slice()) {
      sprite.destroy();
    }
  }

  destroy() {
    this.stop();
    this.clear();
  }

  setSize(width, height, camWidth = null) {
    this.width = width;
    this.height = height;
    this.renderers[0].width = width;
    this.renderers[0].height = height;
    // console.log("set size", width, height, camWidth);
    this.renderers[0].renderer.setSize(width, height);
    this.setCameraWidth(camWidth || width);
  }

  setCameraWidth(width) {
    console.log("setting width", width, this.camera.type, this.camera._type);
    this.cameraWidth = width;
    let aspect = this.height / this.width;
    if (this.camera._type === "ui") {
      let height_ortho = this.cameraWidth * aspect;
      this.camera.left = 0;
      this.camera.right = this.cameraWidth;

      this.camera.top = height_ortho;
      this.camera.bottom = 0;

      // this.camera.top = 0;
      // this.camera.bottom = height_ortho;

      // this.camera.position.z = -100;
      this.camera.updateProjectionMatrix();
      // this.camera.up.set(0, 0, 1);
      // this.camera.lookAt(0, 0, 0);
      console.log(this.camera, this.camera.position);
    } else if (this.camera.type == "OrthographicCamera") {
      let height_ortho = this.cameraWidth * aspect;
      this.camera.left = this.cameraWidth / -2;
      this.camera.right = this.cameraWidth / 2;
      this.camera.top = height_ortho / 2;
      this.camera.bottom = height_ortho / -2;
      this.camera.updateProjectionMatrix();
    } else {
      let fitwidth = this.cameraWidth / this.camera.aspect;
      let fieldOfViewInRadians = this.camera.fov * (Math.PI / 180.0);
      this.camera.position.x = width * 0.0;
      this.camera.position.y = aspect * width * 0.0;
      // this.camera
      // before three update (0.116.1)
      const neg = this.camera.position.z / Math.abs(this.camera.position.z);
      this.camera.position.z =
        neg * ((0.5 * fitwidth) / Math.tan(0.5 * fieldOfViewInRadians));
      this.camera.lookAt(new Vector3(0, 0, 0));
      // this.camera.position.z = (fitwidth) / Math.tan(0.5 * fieldOfViewInRadians);
    }
  }

  getSpriteByName(name) {
    let matched = this.sprites.filter((s) => s.data.name == name);
    if (matched.length > 0) {
      return matched[0];
    }
    return null;
  }

  trigger(name, e) {
    for (let listener of this.listeners) {
      listener(name, e);
    }
  }

  register(callback) {
    this.listeners.push(callback);
  }
}

// if (import.meta.hot) {
//   import.meta.hot.dispose((data) => {
//     console.log("game changed, disposing");
//   });
//   import.meta.hot.accept((newModule) => {
//     if (newModule) {
//       console.log("game updated, restarting");
//     }
//   });
// }
