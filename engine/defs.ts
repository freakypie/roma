import { MagnificationTextureFilter, MinificationTextureFilter } from "three";

export interface GameObjectDef {
  type: "sprite" | "module" | string;
  name?: string;
  blueprint?: string;
  ignore?: boolean;
}

export interface TextureData extends GameObjectDef {
  type: "texture";
  src: string | typeof Image;
  minFilter?: MinificationTextureFilter;
  magFilter?: MagnificationTextureFilter;
  anistropy?: number;
  generateMipMaps?: boolean;
  gradient?: {
    size: number;
    direction: number[];
    stops: number[];
  };
}

export interface BehaviorDef extends Record<string, any> {
  name: string;
}

export interface MeshBehaviorDef extends BehaviorDef {
  name: "three/mesh";
  shape: {
    type:
      | "empty"
      | "polygon"
      | "shape"
      | "circle"
      | "sphere"
      | "square"
      | "box"
      | "cube"
      | "model"
      | "cutout";
    name?: string;
    width: number;
    height: number;
  };
  material?: {
    type:
      | "imported"
      | "basic"
      | "standard"
      | "physics"
      | "toon"
      | "line"
      | "custom";
    name?: string;
  };
}

export interface SpriteDef extends GameObjectDef {
  type: "sprite";
  position: { x: number; y: number; z?: number };
  behaviors: BehaviorDef[];
}
