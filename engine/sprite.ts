/* global THREE */
import { Mesh, Vector3 } from "three";
import type Behavior from "../behaviors/base.js";
import Timeline from "../utils/timeline";
import type { BehaviorDef, SpriteDef } from "./defs.js";
import type Game from "./game.js";
let id = 1;

export interface SpriteData extends Record<string, any> {
  type: "sprite";
  name: string;
  behaviors: BehaviorDef[];
  tags?: string[];
}

export default class Sprite {
  id: number;
  game: Game;
  behaviors: Behavior[] = [];
  mesh?: Mesh;
  body: { id?: number; velocity: Vector3 };
  data: Record<string, any>;
  renderers: any[];
  states: Record<string, any>;
  char: Record<string, number>;
  timeline: Timeline;
  position: Vector3;
  angle: number;
  alive: boolean;
  listeners: Record<string, Function[]>;

  constructor(game: Game, data?: Record<string, any>) {
    this.id = id++;
    this.game = game;
    this.behaviors = [];
    this.mesh = null;
    this.body = { velocity: new Vector3() };
    this.data = {};
    this.renderers = [];
    this.states = {};
    this.char = {};
    if (!game) {
      this.timeline = new Timeline(this);
    } else {
      this.timeline = new Timeline(game);
    }
    this.position = new Vector3();
    this.angle = 0;
    this.alive = true;
    this.listeners = {};

    if (data) {
      // why is it loading in this odd way?
      Sprite.load.bind(this)(data);
    }
  }

  /** create behaviors */
  static async load(game: Game, data: SpriteDef): Promise<Sprite> {
    let sprite = new Sprite(game);
    sprite.data = data;
    if (data.char) {
      sprite.char = data.char;
    }
    if (sprite.data.position) {
      sprite.position.copy(sprite.data.position);
      if (!sprite.position.x) sprite.position.x = 0;
      if (!sprite.position.y) sprite.position.y = 0;
      if (!sprite.position.z) sprite.position.z = 0;
    }
    if (sprite.data.angle) {
      sprite.angle = sprite.data.angle;
    }

    for (let behavior of (data.behaviors || []).filter((c: any) => !c.ignore)) {
      await sprite.addBehavior(behavior);
    }

    return sprite;
  }

  async postLoad() {
    for (let behavior of this.behaviors) {
      if (behavior.postLoad) {
        await behavior.postLoad();
      }
    }
  }

  async addBehavior(behavior: BehaviorDef) {
    let mod = this.game.resolveModule(behavior.name);
    let behave = new mod(this.game, this, behavior);
    await behave.load();
    this.behaviors.push(behave);
    return behave;
  }

  getCharStat(name: string): number {
    return 1 + (this.char[name] || 0) / 5;
  }

  getBehavior<T extends Behavior = Behavior>(name: string): T | undefined {
    return this.behaviors.find((b) => b.data.name == name) as T | undefined;
  }

  trigger(event: string, data: Record<string, any>) {
    let Event = event.charAt(0).toUpperCase() + event.slice(1);
    let handler = `on${Event}`;
    for (let behavior of this.behaviors) {
      // old event interface
      // @ts-ignore
      if (behavior[handler]) {
        // @ts-ignore
        behavior[handler](data);
      }
      behavior.trigger(event, data);
    }
  }

  update(time: number): boolean {
    let remove = [];
    for (let behavior of this.behaviors) {
      if (behavior.update() === false) {
        remove.push(behavior);
      }
    }

    for (let behavior of remove) {
      behavior.destroy();
      let idx = this.behaviors.indexOf(behavior);
      this.behaviors.splice(idx, 1);
    }

    // this.timeline.clean();
    return this.alive;
  }

  draw() {
    for (let renderer of this.renderers) {
      renderer.draw();
    }
    this.timeline.clean();
  }

  despawn() {
    this.alive = false;
    this.states["despawn"] = true;
  }

  destroy() {
    this.alive = false;
    const destroyed: Behavior[] = [];
    for (let behavior of this.behaviors) {
      behavior.destroy();
      destroyed.push(behavior);
    }

    for (let behavior of this.renderers) {
      if (destroyed.includes(behavior)) {
        this.renderers.splice(destroyed.indexOf(behavior), 1);
      }
    }
    let idx = this.game.sprites.indexOf(this);
    this.game.sprites.splice(idx, 1);
    return idx;
  }
}
