import { Manipulation } from "./base.js";
import {
  Vector2,
  Vector3,
  Mesh,
  PlaneGeometry,
  MeshBasicMaterial,
} from "three";
import nudged from "nudged";
import { PI2, PI4 } from "../utils/constants.js";
import { snap } from "../../globals.js";

let SNAP = 30;

export class Resize extends Manipulation {
  get name() {
    return "resize";
  }
  get cursor() {
    return "pointer";
  }

  constructor() {
    super();
    this.mesh = new Mesh(
      new PlaneGeometry(1, 1),
      new MeshBasicMaterial({
        color: "black",
        transparent: true,
        opacity: 0.5,
      }),
    );
    this.mesh.renderOrder = 8000;
    this.offset = new Vector3();
    this.scale = new Vector3();
    this.axis = null;
  }

  manipulate(board) {
    if (board.layerGroup && board.layerGroup != board.canvas) {
      if (this.offset.x === 0) {
        board.helpers.add(this.mesh);
        this.mesh.position.copy(board.layerGroup.mesh.position);
        this.mesh.rotation.z = board.layerGroup.angle;

        this.mesh.scale.x = board.layerGroup.width;
        this.mesh.scale.y = board.layerGroup.height;
      }

      let group = board.layerGroup;
      if (!group.scale) {
        group.scale = 1;
      }
      if (!group.start) {
        group.start = group.mesh.position.clone();
      }
      let start = null;
      let point = null;
      for (let key in board.startInputs) {
        start = this.project(
          board,
          board.startInputs[key].x,
          board.startInputs[key].y,
        );
        point = this.project(board, board.inputs[key].x, board.inputs[key].y);
      }

      if (start && point) {
        let aspect = board.layerGroup.width / board.layerGroup.height;
        if (Math.abs(start.x / aspect) > Math.abs(start.y)) {
          if (start.x > 0) {
            this.scale.x = -(start.x - point.x);
            this.offset.x = -(start.x - point.x);
          } else {
            this.scale.x = start.x - point.x;
            this.offset.x = -(start.x - point.x);
          }
        } else {
          if (start.y > 0) {
            this.scale.y = -(start.y - point.y);
            this.offset.y = start.y - point.y;
          } else {
            this.scale.y = start.y - point.y;
            this.offset.y = start.y - point.y;
          }
        }

        this.scale.x = snap(this.scale.x, SNAP);
        this.scale.y = snap(this.scale.y, SNAP);
        this.offset.x = snap(this.offset.x, SNAP);
        this.offset.y = snap(this.offset.y, SNAP);

        board.helpers.add(this.mesh);
        this.mesh.position
          .copy(board.layerGroup.mesh.position)
          .add(
            this.offset
              .clone()
              .multiplyScalar(0.5)
              .applyAxisAngle(new Vector3(0, 0, 1), board.layerGroup.angle),
          );
        this.mesh.scale.x = board.layerGroup.width + this.scale.x;
        this.mesh.scale.y = board.layerGroup.height + this.scale.y;
      }
    }
  }

  project(board, x, y) {
    let point = new Vector3(
      (x / board.width) * 2,
      (y / board.height) * 2,
      5,
    ).unproject(board.camera);

    // direction to point from camera
    let dir = point.sub(board.camera.position).normalize();
    let distance = -board.camera.position.z / dir.z;

    point = board.camera.position.clone().add(dir.multiplyScalar(distance));

    point.y *= -1;

    // transform to layer
    if (board.layerGroup) {
      point.sub(
        new Vector3(board.layerGroup.position.x, -board.layerGroup.position.y),
      );
      point.applyAxisAngle(new Vector3(0, 0, 1), board.layerGroup.angle);
    }

    return point;
  }

  clean(p, w, h) {
    p.x = Math.max(
      -w / 2,
      Math.min(w / 2, Math.floor((p.x + SNAP / 2) / SNAP) * SNAP),
    );
    p.y = Math.max(
      -h / 2,
      Math.min(h / 2, Math.floor((p.y + SNAP / 2) / SNAP) * SNAP),
    );
  }

  end(board) {
    board.helpers.remove(this.mesh);
    if (board.layerGroup && board.layerGroup != board.canvas) {
      board.layerGroup.resize(
        board.renderer,
        -this.offset.x * 0.5,
        this.offset.y * 0.5,
        board.layerGroup.width + this.scale.x,
        board.layerGroup.height + this.scale.y,
      );

      let pos = board.layerGroup.position
        .clone()
        .add(
          this.offset
            .clone()
            .multiplyScalar(0.5)
            .applyAxisAngle(new Vector3(0, 0, 1), board.layerGroup.angle),
        );
      board.$store.commit("layerUpdate", {
        id: board.layerGroup.id,
        position: {
          x: pos.x, // board.layerGroup.position.x + this.offset.x * 0.5,
          y: pos.y, // board.layerGroup.position.y + this.offset.y * 0.5,
        },
        width: board.layerGroup.width,
        height: board.layerGroup.height,
      });

      this.offset.x = 0;
      this.offset.y = 0;
      this.scale.x = 0;
      this.scale.y = 0;
    }
  }
}
