
export class Manipulation {
  get name() { return "-unknown-"; }
  get cursor() { return "none"; }
  manipulate(board, inputs) {}
  cancel(board) {}
  end(board) {}
}