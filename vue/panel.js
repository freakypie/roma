import { Manipulation } from "./base.js";
import { Vector3, PlaneGeometry, MeshBasicMaterial, Mesh } from "three";

let SNAP = 30;

export class Panel extends Manipulation {
  get name() {
    return "panel";
  }

  constructor() {
    super();
    this.a = null;
    this.b = null;
    this.mesh = new Mesh(
      new PlaneGeometry(1, 1),
      new MeshBasicMaterial({
        color: "black",
        transparent: true,
        opacity: 0.5,
      }),
    );
    this.mesh.renderOrder = 1130;
  }

  manipulate(board) {
    if (Object.keys(board.inputs).length > 0) {
      let input = board.inputs[Object.keys(board.inputs)[0]];
      let point = this.project(board, input.x, input.y);

      if (!this.a) {
        console.log("a", this.a);
        this.a = this.clean(point.clone(), board.width, board.height);
        board.helpers.add(this.mesh);
      }
      this.b = this.clean(point, board.width, board.height);

      let normal = this.a.clone().sub(this.b).normalize();
      let angle = normal.angleTo(
        new Vector3(0, 1, 0).applyAxisAngle(new Vector3(0, 0, 1), board.angle),
      );
      let dist = this.a.distanceTo(this.b);
      this.mesh.position.copy(this.a.clone().add(this.b).multiplyScalar(0.5));
      this.mesh.scale.x = Math.sin(angle) * dist;
      this.mesh.scale.y = Math.cos(angle) * dist;
      this.mesh.rotation.z = board.angle;
    }
  }

  clean(p, w, h) {
    p.x = Math.max(
      -w / 2,
      Math.min(w / 2, Math.floor((p.x + SNAP / 2) / SNAP) * SNAP),
    );
    p.y = Math.max(
      -h / 2,
      Math.min(h / 2, Math.floor((p.y + SNAP / 2) / SNAP) * SNAP),
    );
    return p;
  }

  project(board, x, y) {
    let point = new Vector3(
      (x / board.width) * 2,
      (y / board.height) * 2,
      5,
    ).unproject(board.camera);

    // direction to point from camera
    let dir = point.sub(board.camera.position).normalize();
    let distance = -board.camera.position.z / dir.z;

    point = board.camera.position.clone().add(dir.multiplyScalar(distance));

    return point;
  }

  async end(board) {
    if (
      this.a &&
      Math.abs(this.mesh.scale.x) > 25 &&
      Math.abs(this.mesh.scale.y) > 25
    ) {
      console.error("ending panel");
      this.a = null;
      this.b = null;

      // create layer
      let layerId = await board.$store.dispatch("nextLayerId");
      board.$store.commit("layerAdd", {
        id: layerId,
        type: "group",
        children: [],
        width: Math.abs(this.mesh.scale.x),
        height: Math.abs(this.mesh.scale.y),
        position: this.mesh.position.clone(),
        angle: this.mesh.rotation.z,
      });

      board.$store.commit("layerAdd", {
        type: "paper",
        selected: false,
        group: layerId,
      });
      board.$store.commit("layerAdd", {
        type: "raster",
        selected: true,
        group: layerId,
      });

      board.helpers.remove(this.mesh);

      // reset panel tool
      board.m1 = null;
    }
  }

  cancel(board) {
    this.end(board);
  }
}
