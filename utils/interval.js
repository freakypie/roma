
export default class IntervalMetric {
  constructor(test) {
    this.test = test;
    this.time = null;
  }
  value() {
    let retval = this.test();
    return retval;
  }
  count(time) {
    // console.log("time", time, "this.time", this.time);

    if (this.time === null) return -1;
    return Math.floor((time - this.time) / this.value());
  }
  ready(time) {
    let retval = this.time !== null && (time - this.time) > this.value();
    if (retval) {
      this.time = null;
    }
    return retval;
  }
  start(time) {
    this.time = time;
  }
  stop() {
    this.time = null;
  }
  running() {
    // console.log("running", this.time !== null);
    return this.time !== null;
  }
}
