import { Color, Vector2, Vector3 } from "three";
import Game from "../engine/game";
import { FixedQueue } from "./queue";

export enum Lerpable {
  number = "number",
  color = "color",
  vector3 = "vector3",
  vector2 = "vector2",
}

export interface TimelineNode {
  time: number;
  end: number;
  duration: number;
  value: any;
}

export default class Timeline {
  events: Map<string, FixedQueue>;
  game: Game;

  constructor(game) {
    this.game = game;
    if (!game) {
      console.log("game?", game);
      throw new Error("not a game!");
    }
    this.events = new Map(); //FixedQueue(30);
  }

  clear(key?: string) {
    if (key) {
      if (this.events.has(key)) {
        this.events.get(key).clear();
        delete this.events[key];
      }
    } else {
      delete this.events;
      this.events = new Map();
    }
  }

  add(key, duration, value, delay = 0) {
    if (!this.events.has(key)) {
      this.events.set(key, new FixedQueue(5));
    }

    let node = {
      time: this.game.time + delay,
      end: this.game.time + duration + delay,
      duration: duration,
      value: value,
    };
    this.events.get(key).push(node);
    return node;
  }

  clean() {
    for (const key of this.events.keys()) {
      let queue = this.events.get(key);
      if (queue) {
        for (let x = 0; x < queue.length; x++) {
          const item = queue.head;
          if (item.time + item.duration <= this.game.time) {
            // console.log("clean", key, item);
            queue.shift();
          } else {
            break;
          }
        }
      }
    }
  }

  cleanEvents(key) {
    this.events[key].clear();
  }

  next(key: string, now = 0): TimelineNode | null {
    let event = null;
    let events = this.events.get(key);
    if (events) {
      // console.log(events);
      for (let x = 0; x < events.length; x++) {
        event = events.head;

        // hasn't started
        // if (event.time < this.game.time) {
        // console.log("event hasn't started");
        // event = null;
        // break;

        // already ended
        // } else
        if (event.end >= this.game.time) {
          break;
          // } else {
          // events.shift();
        }
      }
    }
    return event;
  }

  lerp(a: number, event?: TimelineNode) {
    if (event) {
      let b = event.value;
      let p = this.pval(event);
      // console.log("p", a.toFixed(1), b.toFixed(1), p);
      if (p <= 0) return a;
      if (p >= 1) return b;
      return a + (b - a) * p;
    }
    return a;
  }

  lerpObject(
    a: Color | Vector2 | Vector3,
    event: TimelineNode,
  ): Color | Vector2 | Vector3 {
    if (event) {
      let b = event.value;
      let p = this.pval(event);
      // console.log("p", a, b, p);
      if (p <= 0) return a;
      if (p >= 1) return b;
      return a.clone().lerp(b, p);
    }
    return a;
  }

  pval(event: TimelineNode) {
    let p1 = this.game.time;
    let p2 = event.end;
    let d = event.duration;
    let p = 1 - (p2 - p1) / d;
    return p > 1 ? 1 : p;
  }

  lerpProperty(
    key: string,
    type: Lerpable,
    object: any,
    objectKey?: string,
  ): any {
    let nextNode = this.next(key);
    if (nextNode) {
      if (type === Lerpable.number) {
        let startValue = parseFloat(object[objectKey || key]);
        if (!startValue && startValue !== 0) {
          console.error(
            "value is borked?",
            objectKey,
            key,
            object[objectKey || key],
            startValue,
          );
          startValue = 0;
        }
        object[objectKey || key] = this.lerp(startValue, nextNode);
        return object[objectKey || key];
      } else {
        let startValue = object[objectKey || key];
        object[objectKey || key] = this.lerpObject(startValue, nextNode);
        return object[objectKey || key];
      }
    }
    return undefined;
  }
}
