
function scanY(data, fromTop, trimFunc) {
  let offset = fromTop ? 1 : -1;

  // loop through each row
  for (let y = fromTop ? 0 : data.height - 1; fromTop ? (y < data.height) : (y > -1); y += offset) {

    // loop through each column
    for (let x = 0; x < data.width; x++) {
      let rgb = getRBG(data, x, y);
      if (!trimFunc(rgb)) {
        if (fromTop) {
          return y;
        } else {
          return Math.min(y + 1, data.height);
        }
      }
    }
  }
  return null; // all image is white
}

function scanX(data, fromLeft, trimFunc) {
  let offset = fromLeft ? 1 : -1;

  // loop through each column
  for (let x = fromLeft ? 0 : data.width - 1; fromLeft ? (x < data.width) : (x > -1); x += offset) {

    // loop through each row
    for (let y = 0; y < data.height; y++) {
      let rgb = getRBG(data, x, y);
      if (!trimFunc(rgb)) {
        if (fromLeft) {
          return x;
        } else {
          return Math.min(x + 1, data.width);
        }
      }
    }
  }
  return null; // all image is white
};

function getRBG(data, x, y) {
  let offset = data.width * y + x;
  return {
    r: data.data[offset * 4],
    g: data.data[offset * 4 + 1],
    b: data.data[offset * 4 + 2],
    a: data.data[offset * 4 + 3]
  };
}

export function autocrop(imageObject, trimFunc=null) {

  if (! trimFunc) {
    trimFunc = function (rgb) {
      return rgb.a == 0 || (rgb.red == 255 && rgb.green == 255 && rgb.blue == 255);
    };
  }

  let imgWidth = imageObject.width;
  let imgHeight = imageObject.height;
  let canvas = document.createElement('canvas');
  canvas.setAttribute("width", imgWidth);
  canvas.setAttribute("height", imgHeight);

  // draw image on canvas
  let context = canvas.getContext('2d');
  context.drawImage(imageObject, 0, 0);

  let data = context.getImageData(0, 0, imgWidth, imgHeight);
  let top = scanY(data, true, trimFunc);
  let bottom = scanY(data, false, trimFunc);
  let left = scanX(data, true, trimFunc);
  let right = scanX(data, false, trimFunc);
  let width = right - left;
  let height = bottom - top;

  canvas = document.createElement('canvas');
  canvas.setAttribute("width", width);
  canvas.setAttribute("height", height);

  // finally crop the guy
  context = canvas.getContext("2d")
  context.drawImage(imageObject, left, top, width, height, 0, 0, width, height);

  return {
    x: left, y: top, top, left, bottom, right, width, height,
    left_ratio: left / imgWidth,
    top_ratio: top / imgHeight,
    width_ratio: width / imgWidth,
    height_ratio: height / imgHeight,
    previous_width: imgWidth,
    previous_height: imgHeight,
    url: canvas.toDataURL("image/png"),
  };
}