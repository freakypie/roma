
import Behavior from "./base";

export default class TextLabel extends Behavior {

  get defaults() {
    return {
      text: 'hello world',
      style: '',
    };
  }

  constructor(game, sprite, data) {
    super(game, sprite, data);
    this.element = document.createElement('div');
    document.body.appendChild(this.element);

    this.element.style = data.style;
    this.element.innerHTML = data.text;
    this.element.style.position = 'absolute';

    sprite.label = this.element;

    console.log('element', this.element);
  }

  destroy() {
    document.body.removeChild(this.element);
  }

  display() {
    // const pos = this.game.renderer.unproject(
    //   this.sprite.position.x,
    //   this.sprite.position.y,
    // );
    this.element.style.left = this.sprite.position.x;
    this.element.style.top = this.sprite.position.y;
  }
}


TextLabel.register('text_label');