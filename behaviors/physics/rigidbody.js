import Behavior from "../base";
import merge from "deepmerge";
import {Vector3} from "three";


class RigidBodyProxy {
  constructor(game, behavior) {
    this.behavior = behavior;
    this.game = game;
    this.id = null;
    this.mass = 0;
    this.inertia = 0;
    this.angularVelocity = 0;
    this.velocity = new Vector3();
    this.sensor = false;
    this.listeners = [];
  }

  setId(id) {
    this.id = id;
    for (const listener of this.listeners) {
      listener(id);
    }
  }

  waitForId() {
    if (this.id) {
      return this.id;
    }

    const promise = new Promise(r => {
      this.listeners.push(r);
    });
    return promise;
  }

  change(data) {
    data.id = this.id;
    this.game.world.changeBody(data);
  }

  set(data) {
    this.game.world.updateBody({
      id: this.id,
      angle: data.angle,
      x: data.x,
      y: data.y,
      // torque: torque
    });
  }

  impulse(force=null, torque=null) {
    let data = {id: this.id};
    if (force) { data.force = force; }
    if (torque) { data.torque = torque; }
    this.game.world.impulseBody(data);
  }
}


export default class RigidBody extends Behavior {
  constructor(game, sprite, data) {
    super(game, sprite, data);
    sprite.body = new RigidBodyProxy(game, this);
  }

  async load() {
    if (typeof this.data.shape == "object") {
      this.shape = merge({}, this.data.shape);
    } else {
      this.shape = merge({}, this.sprite.data.shapes[this.data.shape]);
    }
    let id = await this.createBody(this.shape);
    this.sprite.body.setId(id);
    this.sprite.body.sensor = this.shape.isSensor;
  }

  async createBody(shape) {
    if (! shape) {
      throw new Error("Invalid shape", this.data);
    }

    // approx measure sprite
    if (shape.radius) {
      this.sprite.radius = shape.radius;
    } else {
      this.sprite.radius = (shape.width + shape.height) / 4;
    }
    return await this.game.world.createBody(this.sprite, shape);
  }

  update() {
    let nextZ = this.sprite.timeline.next("z");
    if (nextZ) {
      let position = this.sprite.position;
      position.z = this.sprite.timeline.lerp(position.z, nextZ);
      this.game.world.updateBody({id: this.sprite.body.id, x: position.x, y: position.y, z: position.z});
    }

    let next = this.sprite.timeline.next("body_mask");
    if (next && next.body_mask != this.shape.mask) {
      this.shape.mask = next.body_mask;
      this.game.world.changeBody({id: this.sprite.body.id, shape: this.shape});
    }

    let nextAngle = this.sprite.timeline.next("bodyAngle");
    if (nextAngle && nextAngle.value != this.sprite.angle) {
      this.sprite.body.set({angle: nextAngle.value});
    }
  }

  destroy() {
    this.game.world.destroyBody(this.sprite);
    this.sprite.body = null;
  }
}
