// PointContainer = require '../utils/points'
// IntervalMetric = require "../utils/timer"
// Sprite = require '../sprite'
// mod = require("../utils/modifier").default
import deepmerge from "deepmerge";
import {Vector3} from "three";
import IntervalMetric from "../utils/interval";
import Behavior from "./base";


function rand(x=1) {
  return Math.random() * x * 2 - x;
}


export default class Weapon extends Behavior {
  get defaults() {
    return {
      x: 0,
      y: 0,
      damage: 10,
      bullet_speed: 10,
      bullet_size: 0.05,
      max_duration: 2000,
      inaccuracy: 0.05,
      fire_knockback: 0.1,
      fire_interval: 0.6,
      burst_interval: 0.07,
      burst_size: 3,
      clip_interval: 1.00,
      clip_size: 9,
      pattern_scale: 5.0,
      pattern_initial_scale: 0.1,
    };
  }

  constructor(game, sprite, data={}) {
    super(game, sprite, data);

    if (this.data.pattern) {
      let newPattern = [];
      for (let p of this.data.pattern) {
        p = new Vector3(p[0], p[1]);
        p.add(new Vector3(0, 0.5));
        newPattern.push(p);
      }
      this.data.pattern = newPattern;
    }
    this.patternIndex = 0;

    // ammo
    this.clip = 0;  // this.getValue("clip_size");
    this.burst = 0;  // this.getValue("burst_size");
    this.clipInterval = new IntervalMetric(
      () => this.getValue("clip_interval") * 1000
    );
    this.fireInterval = new IntervalMetric(
      () => this.getValue("fire_interval") * 1000
    );
    this.burstInterval = new IntervalMetric(
      () => this.getValue("burst_interval") * 1000
    );
  }

  getValue(trait) {
    return this.sprite.getCharStat(trait) ** 2 * this.data[trait];
  }

  forwardVector(scale, offset=0) {
    let vec = new box2d.b2Vec2(0, scale);
    if (scale !== 0) {
      vec.SelfRotateRadians(this.sprite.body.GetAngleRadians() + offset);
    }
    return vec;
  }

  reload() {
    this.clipInterval.start(this.game.time);
    this.clip = 0;
  }

  update() {
    // clip ready?
    if (this.clipInterval.ready(this.game.time)) {
      this.clip = this.getValue("clip_size");
      this.patternIndex = 0;
      this.clipInterval.stop();
    }

    if (this.clip > 0) {

      // next chamber ready?
      if (this.fireInterval.ready(this.game.time)) {
        this.burst = Math.min(this.clip, this.getValue("burst_size"));
        this.fireInterval.stop();
      } else if(this.burst <= 0 && !this.fireInterval.running()) {
        this.burstInterval.stop();
        this.fireInterval.start(this.game.time);
      }
    }

    if (this.sprite.states.fire || this.burstInterval.running()) {
      this.fire();
    } else if (this.clip == 0 && ! this.clipInterval.running()) {
      this.reload();
    }
  }

  fire() {
    if (this.clip > 0) {

      if (this.burst > 0) {
        if (! this.burstInterval.running()) {
          this.burstInterval.start(this.game.time);
        }

        let burstCount = this.burstInterval.count(this.game.time);
        if (burstCount > 0) {
          // count how many fire
          let count = Math.min(this.burst, burstCount);
          if (count > 0) {
            this.doFire(count);
            this.burstInterval.start(this.game.time);

            // remove fired
            this.burst -= count;
            this.clip -= count;
            return true;
          }
        }
      }
    } else if (! this.clipInterval.running()) {
      this.reload();
    }

    return false;
  }

  angleTo(target) {
    target = target.clone().normalize();
    let angle = Math.atan2(-target.x, target.y);
    if (Math.abs(angle) > Math.PI) {
      if (angle > 0) {
        angle = (angle - Math.PI * 2)
      } else {
        angle = (angle + Math.PI * 2)
      }
    }
    return angle;
  }

  doFire(count) {
    // todo: make sound
    // Media.sound(this.sound_name, this.sprite.body.GetPosition())

    let knockback = -this.getValue("fire_knockback");
    let inaccuracy = this.getValue("inaccuracy");
    let speed = this.getValue("bullet_speed");
    let size = this.getValue("bullet_size");
    let damage = this.getValue("damage");

    for (let idx=0; idx<count; idx+=1) {

      // make inaccurate
      let effectiveAngle = this.sprite.angle + rand(inaccuracy);
      let direction = new Vector3(0, 1, 0).applyAxisAngle(
        new Vector3(0, 0, 1),
        effectiveAngle
      ).add(this.sprite.body.velocity.clone().multiplyScalar(0.01))
       .multiplyScalar(speed * this.sprite.body.inertia);

      let position = this.sprite.position.clone().add(
        direction.clone().normalize().multiplyScalar(
          this.sprite.radius * 1.1
        )
      );//kickback_at.clone(); // .SelfAdd(unit);

      let pattern = null;
      if (this.data.pattern) {
        // update position and direction based on preset pattern
        pattern = this.data.pattern[this.patternIndex].clone().applyAxisAngle(
          new Vector3(0, 0, 1), effectiveAngle);
        this.patternIndex = (this.patternIndex + 1) % this.data.pattern.length
      } else {
        // pattern = new Vector3(rand(inaccuracy), rand(inaccuracy), 0);
        pattern = new Vector3(0, 0, 0);
      }
      direction.add(pattern.clone().multiplyScalar(this.data.pattern_scale));
      position.add(pattern.clone().multiplyScalar(this.data.pattern_initial_scale));

      this.sprite.body.impulse(direction.clone().multiplyScalar(knockback));

      effectiveAngle = this.angleTo(direction);

      // console.log("impulse", direction);
      let data = {
        extends: this.data.blueprint,
        position: {x: position.x, y: position.y, z: position.z},
        force: {x: direction.x, y: direction.y, z: direction.z},
        scale: size,
        angle: effectiveAngle,
        group: this.sprite.data.group
      };

      this.game.load([data]).then((bullets) => {
        let bullet = bullets[0];
        // console.log(bullet);
        // this.game.world.impulseBody({
        //   id: bullet.body.id,
        //   // force: {x: direction.x, y: direction.y},
        //   torque: rand(0.5)
        // });
        // TODO: bullet.shooter = this.sprite;
      });
    }
  }
}
