import Behavior from "./base";

export default class SpawnBehavior extends Behavior {

  get defaults() {
    return {
      interval: 1000,
      count: null
    };
  }

  constructor(game, sprite, data) {
    super(game, sprite, data);
    this.start = game.time;
    this.count = 0;
    this.spawns = [];
  }

  update() {
    if (! this.sprite.wire || this.sprite.wire.current) {
      if (this.data.interval && this.game.time - this.start > this.data.interval) {
        this.start = this.game.time;
        this.count += 1;
        this.spawn();

        // console.log("spawn", this.data.count, this.count);aaa
        if (this.data.count && this.count >= this.data.count) {
          return false;
        }
      }
    }
    this.spawns = this.spawns.filter(s => s.alive);
  }

  async spawn() {
    let pos = this.sprite.position.clone();
    // pos.z += 1;
    console.log("spawn group", this.sprite.data.group)
    let data = {
      "type": "sprite",
      "extends": this.data.blueprint,
      "group": Math.abs(this.sprite.data.group),
      "position": pos
    };
    let retval = (await this.game.load([data]))[0];

    console.log("spawn", this.sprite.wire);
    return retval;
  }
}
