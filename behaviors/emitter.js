import {Vector3, Color, AdditiveBlending} from "three";
import SPE from "shader-particle-engine";
import Behavior from "./base";


export default class ParticleEmitterBehavior extends Behavior {

  get defaults() {
    return {
      name: "default",
      position: new Vector3(),
      spawn_radius: 0.5,
      velocity_spread: 0.001,
      size: 0.9,
      size_spread: 0.0,
      count: 10,
      acceleration: 0.0001,
      max_age: 200,
      max_age_spread: 50,
      wiggle: 0.0,
      wiggle_spread: 0.0,
      duration: 1000,
      drag: 0.0,
      drag_spread: 0.0, //0.1,
      angle: 3,
      angle_spread: 3,
      rotation: 0,
      rotation_spread: 0,
      opacity: 0.7,
      opacity_spread: 0.3,
      colors: ["white", "blue"],
      texture: "white"
    };
  }

  get emitterSettings() {
    let velocity = new Vector3();
    if (this.data.velocity) {
      velocity = velocity.fromArray(this.data.velocity);
    }
    return {
      type: SPE.distributions.BOX,
      particleCount: this.data.count,
      duration: null, //this.data.duration / 1000.0,
      maxAge: {
        value: this.data.max_age, // 1000.0,
        spread: this.data.max_age_spread, // 1000.0
      },
      position: {
        spread: new Vector3(
          this.data.spawn_radius, this.data.spawn_radius, 0)
      },
      acceleration: {
        value: new Vector3(0, 0, 0),
        spread: new Vector3(
          this.data.acceleration / 100, this.data.acceleration / 100, 0)
      },
      velocity: {
        value: velocity,
        spread: new Vector3(
          this.data.velocity_spread, this.data.velocity_spread, 0)
      },
      drag: {
        value: this.data.drag,
        spread: this.data.drag_spread
      },
      rotation: {
        axis: new Vector3(0, 0, 1),
        center: new Vector3(0, 0, 0),
        angle: this.data.rotation,
        angleSpread: this.data.rotation_spread
      },
      angle: {
        value: this.data.angle,
        spread: this.data.angle_spread
      },
      wiggle: {
        value: this.data.wiggle,
        spread: this.data.wiggle_spread
      },
      opacity: {
        value: this.data.opacity,
        spread: this.data.opacity_spread
      },
      color: {value: this.data.colors.map((a) => new Color(a))},
      size: {
        value: this.data.size * window.devicePixelRatio,
        spread: this.data.size_spread * window.devicePixelRatio
      },
    };
  }

  constructor(game, sprite, data) {
    super(game, sprite, data);
    ParticleEmitterBehavior.register(this.data.group_name, this);

    this.sprite.renderers.push(this);
  }

  static register(name, group) {
    if (! this._registry) {
      this._registry = {};
    }
    this._registry[name] = group;
  }

  static get(name) {
    return this._registry[name];
  }

  async load() {
    this.group = new SPE.Group({
      maxParticleCount: 2000,
      hasPerspective: true,
      // TODO
      texture: {
        value: this.game.textures[this.data.texture]
      },
      blending: AdditiveBlending,
      transparent: true
    });

    this.group.addPool(10, this.emitterSettings, true);
    this.game.scene.add(this.group.mesh);
    this.group.mesh.frustumCulled = false;
  }

  emit(pos, vel) {
    let emitter = this.group.getFromPool();
    if (! emitter.group) {
      // this is a fresh emitter
      this.group.addEmitter(emitter);
    }
    emitter._start = this.game.time;
    emitter.enable();
    if (pos) { emitter.position.value = pos.clone(); }
    if (vel) { emitter.velocity.value = vel.clone(); }

    return emitter;
  }

  draw() {
    this.group.tick(1000 / 60);
  }

  // create(game, options={}) {
  //   options = _.defaults(options, {
  //     create_radius: 0.25,
  //     number: 3,
  //     numberSparks: 2,
  //     speed: 1,
  //     angular_velocity: 10,
  //     life: 50,
  //     scale: 0.5,
  //     scaleVariation: 0.1,
  //     duration: 1000,
  //     material: "particle",
  //   });
  //
  //   let particles = new THREE.Geometry();
  //   let spacing = options.create_radius * 1000.0;
  //   let vels = [];
  //   for (let num=0; num<options.number; num++) {
  //
  //     let vector = new THREE.Vector3(
  //       _.random(-spacing, spacing) / 1000.0,
  //       _.random(-spacing, spacing) / 1000.0,
  //       _.random(-spacing, spacing) / 1000.0
  //     );
  //     // particles.vertices.push(vector);
  //     let vel = vector.clone().normalize().multiplyScalar(options.speed);
  //     game.createSprite({
  //       position: options.position.clone().add(vector),
  //       angle: Math.random() * Math.PI,
  //       scale: 0.1,
  //       local: true,
  //       behaviors: [
  //         {name: "sprite-renderer", material: options.material},
  //         {
  //           name: "rigid-body",
  //           sensor: true,
  //           dx: vel.x,
  //           dy: vel.y,
  //           // linearDamping: 1,
  //           // angularDamping: 1,
  //           // category: 0b00100,
  //           // mask: 0b10000
  //         },
  //         {name: "mortal", duration: options.duration}
  //       ]
  //     });
  //   }
  // }
}
