import Behavior from "../base";
import {PointLight} from "three";



export default class LightBehavior extends Behavior {

  get defaults() {
    return {
      color: 0xffffff,
      intensity: 2,
      radius: 5,
      height: 1
    };
  }

  async load() {
    let light = new PointLight(
      this.data.color,
      this.data.intensity,
      this.data.radius
    );

    this.light = light;
    this.light.userData = this.sprite.id;
    this.light.castShadow = true;
    this.light.shadow.mapSize.width = 1024;
    this.light.shadow.mapSize.height = 1024;
    this.game.scene.add( this.light );
    this.light.position.copy(this.sprite.position);
    this.light.position.z += this.data.height;
  }

  update() {
    return true;
  }

  destroy() {
    console.log("destrying")
    this.game.scene.remove(this.light);
  }
}

LightBehavior.lastLight = 0;
