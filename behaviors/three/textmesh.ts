import { Vector3, Quaternion, Euler } from "three";
import Behavior from "../base";

const FALLBACK_FONTS = [
  `/assets/fonts/Symbola.ttf`,
  `/assets/fonts/OpenSansEmoji.ttf`,
  `/assets/fonts/fireflysung.ttf`,
];

export default class TextMesh extends Behavior {
  // extends Mesh {

  constructor(game, sprite, data) {
    super(game, sprite, data);

    this.sprite.mesh = this.createMesh();
    if (!data.static) {
      this.sprite.renderers.push(this);
    }
  }

  createMesh() {
    let mesh = new Text();
    this.game.scene.add(mesh);

    console.log("font", this.data.font);

    mesh.text = this.data.text;
    // mesh.font = this.data.font || '/assets/fonts/Symbola.ttf';
    // `http://localhost:8080/assets/fonts/fireflysung.ttf`;
    // `http://localhost:8080/assets/fonts/Symbola.ttf`;
    mesh.fontSize = this.data.fontSize || "0.9em";
    mesh.color = this.data.color || "#000000";
    mesh.strokeOpacity = this.data.opacity || 1;
    mesh.fillOpacity = this.data.opacity || 1;
    mesh.outlineColor = this.data.outlineColor;
    mesh.outlineBlur = this.data.outlineBlur;
    mesh.outlineOpacity = this.data.outlineOpacity;
    mesh.visible = !this.data.invisible;
    mesh.anchorX = "50%";
    mesh.anchorY = "50%";
    // mesh.debugSDF = true;
    console.log("text", mesh);
    // mesh.material = material;

    // if ((this.data.shadows === undefined || this.data.shadows) && ! this.data.material.emissive) {
    //   mesh.castShadow = true;
    //   mesh.receiveShadow = true;
    // }
    mesh.position.copy(this.sprite.position);
    // mesh.rotation.z = this.sprite.angle;
    // if (this.data.visible !== undefined) {
    //   mesh.visible = this.data.visible;
    // }

    let scale = this.data.scale || 1;
    if (typeof scale === "object") {
      mesh.scale.set(scale.x, scale.y, scale.z);
    } else {
      mesh.scale.set(scale, scale, scale);
    }

    return mesh;
  }

  async getFailIndex() {
    let retval = TextMesh.failIndices[this.data.font];
    if (!retval) {
      let mesh = new Text();
      mesh.text = "語";
      mesh.font = this.data.font;
      await new Promise((r) => mesh.sync(r));
      retval = mesh.textRenderInfo.glyphAtlasIndices[0];
      // console.log("missing char index is", retval);
    }
    return retval;
  }

  async load() {
    await new Promise((r) => this.sprite.mesh.sync(r));
    // console.log(this.sprite.data.char, this.sprite.mesh.textRenderInfo.glyphAtlasIndices[0]);
    // if (this.sprite.mesh.textRenderInfo.glyphAtlasIndices[0] === await this.getFailIndex()) {
    //   for (const fallback of FALLBACK_FONTS) {
    //     // console.log('missing char', this.sprite.data.char, this.data.font);
    //     this.data.font = fallback;
    //     this.sprite.mesh.font = fallback;
    //     await new Promise(r => this.sprite.mesh.sync(r));
    //     if (
    //       this.sprite.mesh.textRenderInfo.glyphAtlasIndices[0] !== await this.getFailIndex()
    //     ) {
    //       // console.log('missing char found in fallback', this.sprite.data.char);
    //       break;
    //     }
    //   }
    // }
  }

  draw() {
    if (!this.data.static && this.sprite.mesh) {
      let pos = this.sprite.position.clone();
      let angle = this.sprite.angle;
      let orientation = this.sprite.orientation;

      if (!this.target) {
        let next = this.sprite.timeline.next("position");
        if (next) {
          this.target = next;
        }
      }
      if (this.target) {
        let p = this.sprite.timeline.pval(this.target);
        if (p > 0) {
          pos.lerp(this.target.value.position, p);

          let b = new Quaternion();
          b.setFromAxisAngle(new Vector3(0, 0, 1), this.target.value.angle);
          let a = this.sprite.mesh.quaternion.clone().slerp(b, p);
          angle = new Euler().setFromQuaternion(a).z;
          if (p >= 1) {
            this.sprite.position.copy(this.target.value.position);
            this.sprite.angle = angle;
            this.target = null;
          }
        }
      }

      this.sprite.mesh.position.copy(pos);
      if (orientation) {
        this.sprite.mesh.quaternion.copy(orientation);
      } else {
        this.sprite.mesh.rotation.z = angle;
      }

      let nextOpacity = this.sprite.timeline.next("opacity");
      if (nextOpacity) {
        if (nextOpacity.start === undefined) {
          nextOpacity.start = this.sprite.mesh.material.opacity;
        }
        this.sprite.mesh.material.opacity = this.sprite.timeline.lerp(
          nextOpacity.start,
          nextOpacity
        );
      }

      let nextScale = this.sprite.timeline.next("scale");
      if (nextScale) {
        if (!nextScale.start) {
          nextScale.start = this.sprite.mesh.scale.x;
        }
        this.sprite.mesh.scale.x = this.sprite.timeline.lerp(
          nextScale.start,
          nextScale
        );
        this.sprite.mesh.scale.y = -this.sprite.mesh.scale.x;
        this.sprite.mesh.scale.z = this.sprite.mesh.scale.x;
      }
    }
  }

  update() {
    return true;
  }

  destroy() {
    this.game.scene.remove(this.sprite.mesh);
    // TODO: this.sprite.mesh.geometry.dispose();
    // TODO: this.sprite.mesh.material.dispose();
    if (this.sprite.mesh) {
      this.sprite.mesh.dispose();
      // this.sprite.mesh = null;
    }
  }
}
TextMesh.failIndices = {};

TextMesh.register("three/textmesh");
