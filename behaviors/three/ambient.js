import Behavior from "../base";
import {AmbientLight, HemisphereLight} from "three";

export default class AmbientLightBehavior extends Behavior {

  get defaults() {
    return {
      color: "skyblue",
      color2: "white",
    };
  }

  constructor(game, sprite, data) {
    super(game, sprite, data);
  }

  async load() {
    this.light = new HemisphereLight(this.data.color, this.data.color2);
    this.game.scene.add(this.light);
  }

  update() {
    this.light.position.copy(this.sprite.position);
  }

  destroy() {
    this.game.scene.remove(this.light);
  }
}
