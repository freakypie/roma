import {Bone, Skeleton, SkeletonHelper, Vector3} from "three";
import Behavior from "../base";


function ease(a, b, p, func='easeInOutQuart') {
  const t = {
    easeInOutQuart: (x) => x < 0.5 ? 8 * x * x * x * x : 1 - Math.pow(-2 * x + 2, 4) / 2,
  }[func](p);
  return a + (b - a) * t;
}


class SkeletonProxy {
  constructor(game, behavior) {
    this.behavior = behavior;
    this.game = game;
  }

  get root() {
    return this.behavior.children[0].body;
  }

  get id() {
    return this.root.id;
  }

  get isSkeleton() {
    return true;
  }

  setId(id) {
    this.root.setId(id);
  }

  waitForId() {
    return this.root.waitForId();
  }

  change(data) {
    this.root.change(data);
  }

  set(data) {
    this.root.set(data);
  }

  // impulse(force=null, torque=null) {
  //   this.root.set(impulse);
  // }
}

export default class ClothSkeleton extends Behavior {

  get defaults() {
    return {
    };
  }

  constructor(game, sprite, data) {
    super(game, sprite, data);
    this.sprite.renderers.push(this);
  }

  async load() {
    let pos = this.sprite.position.clone();
    let root = new Bone(new Vector3());
    root.pos = pos;
    root._opts = { weld: true };

    let bones = [root];
    let bone = null;
    let prev = null;
    for (let {position, parent, id, opts} of this.data.bones) {
      bone = new Bone();
      bone._id = id;
      bone._parent = parent;
      bone._opts = {};
      for (const prop in opts) {
        bone._opts[prop] = opts[prop];
      }
      bone.pos = position;
      bones.push(bone);
    }
    for (const bone of bones.slice(1)) {
      if (bone._parent) {
        prev = bones.find(b => b._id === bone._parent);
      } else {
        prev = root;
      }
      bone.position.x = bone.pos.x - prev.pos.x;
      bone.position.y = bone.pos.y - prev.pos.y;
      prev.add(bone);
    }

    // create skinned mesh and skeleton
    let skeleton = new Skeleton( bones );

    // bind the skeleton to the mesh
    if (this.sprite.mesh.bind) {
      this.sprite.mesh.add( root );
      this.sprite.mesh.bind( skeleton );

      this.root = root;
      this.skeleton = skeleton;
      this.sprite.skeleton = skeleton;

      // TODO: REMOVE DEBUGGING
      this.helper = new SkeletonHelper( this.sprite.mesh );
      this.helper.material.linewidth = 3;
      this.game.scene.add( this.helper );
    } else {
      console.error('mesh is not bindable');
    }

    this.sprite.body = new SkeletonProxy(this.game, this);
  }

  async postLoad() {
    this.children = await this.spritizeBone(this.root);
  }

  async spritizeBone(bone, children = []) {
    let world1 = this.sprite.position.clone();
    let world2 = new Vector3();
    let base = null;
    let dir = null;
    let angle = null;
    let prev = this.data.parent;

    bone.getWorldPosition(world2);

    if (bone.parent) {
      base = bone.parent;

      base.getWorldPosition(world1);
      dir = world2.clone().sub(world1).normalize();
      angle = Math.atan2(-dir.x, dir.y);
      if (Math.abs(angle) > Math.PI) {
        if (angle > 0) {
          angle = (angle - Math.PI * 2);
        } else {
          angle = (angle + Math.PI * 2);
        }
      }

      if (bone.parent instanceof Bone) {
        prev = "bone-" + base.id;
      } else {
        prev = this.data.parent;
      }
    } else {
      world2 = this.sprite.position.clone().add(bone.position);
    }

    let boneid = "bone-" + bone.id; // this is the THREE.js ID
    let dist = world2.distanceTo(world1);
    if (dist === 0) {
      dist = 0.05;
    }
    const pos = world2.clone().add(world1).multiplyScalar(0.5);
    const spring = bone._opts.spring;
    const damp = ease(0, 1, bone._opts.dampening);

    const extra = [];
    if (bone._opts.weld) {
      extra.push({
        name: "physics/joint", type: "weld",
        a: "self", b: this.data.parent,
        position: {x: world1.x, y: world1.y},
        frequencyHz: 0.0,
        dampingRatio: 0,
      });
    } else {
      const freq = ease(10, 30, spring);
      extra.push({
        name: "physics/joint",
        // type: "revolute",
        type: spring > 0.1 ? "weld" : "revolute",
        a: "self", b: prev,
        position: {x: world1.x, y: world1.y},
        frequencyHz: freq,
        dampingRatio: 0.0,
      });
      // extra.push({
      //   name: "physics/joint",
      //   type: "distance",
      //   // type: spring > 0.1 ? "weld" : "revolute",
      //   a: "self", b: prev,
      //   position: {x: world1.x, y: world1.y},
      //   position2: {x: world2.x, y: world2.y},
      //   frequencyHz: 30,
      //   dampingRatio: 0.5,
      // });
    }

    const damping = ease(2, 100.0, damp);
    const size = (bone._opts.size || dist + 0.01);
    const radius = (size + dist) / 2;
    let result = await this.game.load([
      {
        type: "sprite",
        name: boneid,
        isBone: true,
        weld: bone._opts.weld,
        group: 1,
        position: {x: world2.x, y: world2.y, z: 0.01},
        angle: angle,
        behaviors: [
          // {
          //   name: "three/mesh",
          //   shape: {
          //     // type: 'box', width: size, height: dist + 0.01,
          //     type: 'circle', radius,
          //     center: { x: 0, y: dist / 2 },
          //   },
          //   material: {color: bone._opts.weld ? 'grey' : 'red', transparent: true, opacity: 0.25}
          // },
          {
            name: "physics/rigidbody",
            shape: {
              mask: '1101',
              category: '0010',
              angle: angle,
              density: size,
              damping,
              // type: 'box', width: size, height: dist + 0.01,
              type: 'circle', radius: (size + dist) / 2,
              gravityScale: 0,
              radius: dist,
              center: { x: 0, y: -dist / 2 },
            }
          },
          {
            name: 'physics/gravity',
            x: 0,
            y: -0.09
          },
          ...extra
        ],
      },
    ]);
    const sprite = result[0];
    sprite._bone = bone;
    sprite._angle = angle;
    bone.sprite = sprite;
    children.push(sprite);

    for (let x = 0; x < bone.children.length; x += 1) {
      await this.spritizeBone(bone.children[x], children);
    }

    return children;
  }

  draw () {}

  update() {
  //   return;
  // };

  // draw() {
    if (this.children) {
      // move the bones and manipulate the model
      let parent = null;
      let world = new Vector3();
      let count = 0;
      for (let child of this.children) {
        child._bone.rotation.z = child.angle - child._angle;
        if (child._bone.parent instanceof Bone) {
          parent = child._bone.parent.sprite;
          child._bone.rotation.z -= parent.angle - parent._angle; //parent._bone.rotation.z;
          // if (child.data.weld || parent.data.weld) {
            world = parent._bone.worldToLocal(child.position.clone());
            child._bone.position.x = world.x;
            child._bone.position.y = world.y;
            child._bone.updateWorldMatrix(false, false);
            // }
        } else {
          child._bone.rotation.z -= this.sprite.angle;
          world = this.sprite.mesh.worldToLocal(child.position.clone());
          child._bone.position.x = world.x;
          child._bone.position.y = world.y;
          child._bone.updateWorldMatrix(false, false);
        }
        count += 1;
      }
    }
  }

  destroy() {
    if (this.helper) {
      this.game.scene.remove(this.helper);
    }
    // destroy all children
    for (const child of this.children) {
      child.alive = false;
    }
  }
}


function angleBetween(a, b) {
  let dir = b.clone().sub(a).normalize();
  let angle = Math.atan2(-dir.x, dir.y);
  if (Math.abs(angle) > Math.PI) {
    if (angle > 0) {
      angle = (angle - Math.PI * 2);
    } else {
      angle = (angle + Math.PI * 2);
    }
  }
  return angle;
}