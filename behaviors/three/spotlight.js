import Behavior from "../base";
import {SpotLight} from "three";


export default class SpotLightBehavior extends Behavior {

  get defaults() {
    return {
      color: 0xffffff,
      intensity: 2,
      distance: 5,
      angle: Math.PI / 3,
      penumbra: 0,
      decay: 1,
      height: 1
    };
  }

  constructor(game, sprite, data) {
    super(game, sprite, data);
  }

  async load() {
    let light = new SpotLight(
      this.data.color,
      this.data.intensity,
      this.data.distance,
      this.data.angle,
      // this.data.penumbra,
      // this.data.decay
      // this.data.radius
    );
    light.castShadow = true;

    light.shadow.mapSize.width = 1024;
    light.shadow.mapSize.height = 1024;

    // light.shadow.camera.near = 0;
    // light.shadow.camera.far = 400;
    // light.shadow.camera.fov = 30;

    light.target.position.copy(this.sprite.position);
    light.target.position.z = 0;

    this.light = light;

    this.game.scene.add( this.light );
    this.game.scene.add( this.light.target );
    this.light.position.copy(this.sprite.position);
    this.light.position.z += this.data.height;
  }

  update() {
    let camera = this.game.camera.position.clone();
    camera.z = this.light.position.z;
    let dist = this.light.position.distanceTo(camera);
    let last = this.light.visible;
    this.light.visible = (dist > this.game.cameraWidth + this.light.distance - 1) ? false : true;
    if (last != this.light.visible) {
      console.log("light visibility changed", this.light.visible);
    }
    // console.log(dist, this.light.distance)

    this.light.position.copy(this.sprite.position);
    this.light.position.z += this.data.height;

    let next = this.sprite.timeline.next("light_intensity");
    if (next) {
      this.light.intensity = this.data.intensity * this.sprite.timeline.lerp(
        this.light.intensity / this.light.intensity,
        next);
    }

    // next = this.sprite.timeline.next("light_radius");
    // if (next) {
    //   this.light.distance = this.sprite.timeline.lerp(this.light.radius, next);
    // }
  }

  destroy() {
    this.game.scene.remove(this.light);
  }
}
