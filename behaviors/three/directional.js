import Behavior from "../base";
import {DirectionalLight, SpotLight, CameraHelper} from "three";



export default class LightBehavior extends Behavior {

  get defaults() {
    return {
      color: 0xffffff,
      intensity: 2,
      radius: 5,
      height: 5
    };
  }

  async load() {
    // let light = new SpotLight(
    let light = new DirectionalLight(
      this.data.color,
      this.data.intensity,
      100,
    );

    light.castShadow = true;
    light.shadow.camera.near = 0.5;
    light.shadow.camera.far = 1000;
    let size = 16 + 8;
    light.shadow.camera.right = size;
    light.shadow.camera.left = -size;
    light.shadow.camera.top	= size;
    light.shadow.camera.bottom = -size;
    light.shadow.mapSize.width = 2048;
    light.shadow.mapSize.height = 2048;

    this.light = light;
    this.updateLightPosition(this.sprite.position);
    this.light.userData = this.sprite.id;
    this.game.scene.add(this.light);
    this.game.scene.add(this.light.target);
    // this.game.scene.add(new CameraHelper(light.shadow.camera));
  }

  update() {
    // this.updateLightPosition(this.game.camera.position);
  }

  updateLightPosition(position) {
    this.light.position.copy(position);  // this.data.height;
    this.light.position.y = this.data.height;
    this.light.position.z = this.data.height;
    this.light.position.x += this.data.height;
    this.light.target.position.z = 0;
    this.light.target.position.x = 0;
    this.light.target.position.y = 0;
  }

  destroy() {
    // this.game.scene.remove(this.light);
  }
}

LightBehavior.lastLight = 0;
