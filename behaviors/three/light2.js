import Behavior from "../base";
import {PointLight} from "three";


const MAX_LIGHTS = 12;

export default class LightBehavior extends Behavior {

  get defaults() {
    return {
      color: 0xffffff,
      intensity: 2,
      radius: 5,
      height: 1
    };
  }

  constructor(game, sprite, data) {
    super(game, sprite, data);
    if (! game.lights) {
      game.lights = [];
    }
    this.lightIndex = null;
    this.lightUsed = false;
  }

  async load() {
    if (this.game.lights.length < MAX_LIGHTS) {
      let light = new PointLight(
        this.data.color,
        0,
        this.data.radius
      );

      this.light = light;
      this.light.userData = this.sprite.id;
      // this.light.castShadow = true;
      // this.light.shadow.mapSize.width = 1024;
      // this.light.shadow.mapSize.height = 1024;
      this.game.scene.add( this.light );
      this.light.position.copy(this.sprite.position);
      this.light.position.z += this.data.height;

      this.lightIndex = this.game.lights.length;
      this.lightUsed = true;
      this.game.lights.push(light);
      LightBehavior.lastLight += 1;
    } else {
      this.lightUsed = false;
    }
  }

  update() {
    let camera = this.game.camera.position.clone();
    camera.z = this.sprite.position.z;
    let dist = this.sprite.position.distanceTo(camera);
    let last = this.lightUsed;
    this.lightUsed = (dist > this.game.cameraWidth + this.data.radius - 1) ? false : true;
    if (this.lightUsed != last) {
      if (this.lightUsed) {
        if (!this.light || this.light.userData != this.sprite.id) {
          // get an available light and reset it's properties
          this.lightIndex = (LightBehavior.lastLight++) % this.game.lights.length;
          this.light = this.game.lights[this.lightIndex];
          this.light.userData = this.sprite.id;
          console.log("recyclyed light");
        }
        this.light.visible = true;
      } else {
        // if (this.light.userData === this.sprite.id) {
        //   this.light.visible = false;
        // }
      }
      console.log("light visibility changed", this.lightUsed);
    }

    if (this.light && this.light.userData === this.sprite.id) {
      this.light.position.copy(this.sprite.position);
      this.light.position.z += this.data.height;

      let next = this.sprite.timeline.next("light_intensity");
      if (next) {
        this.light.intensity = this.data.intensity * this.sprite.timeline.lerp(
          this.light.intensity / this.data.intensity,
          next);
      }
    }
  }

  destroy() {
    this.game.scene.remove(this.light);
  }
}

LightBehavior.lastLight = 0;
