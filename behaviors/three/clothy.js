import {
  Bone,
  BufferAttribute,
  Color,
  Float32BufferAttribute,
  Line3,
  Uint16BufferAttribute,
  Vector2,
  Vector3,
} from "three";

import Behavior from "../base";
import sortby from "sort-by";

export default class Clothy extends Behavior {
  get defaults() {
    return {};
  }

  constructor(game, sprite, data) {
    super(game, sprite, data);
  }

  async load() {
    let geometry = this.sprite.mesh.geometry;
    if (geometry && this.sprite.skeleton) {
      // create the skin indices and skin weights
      let position = geometry.attributes.position;
      let vertex = new Vector3();

      let skinIndices = [];
      let skinWeights = [];

      for (const bone of this.sprite.skeleton.bones) {
        if (bone.parent instanceof Bone) {
          const parent = bone.parent;
          const p1 = new Vector3(
            bone.pos.x - this.sprite.position.x,
            bone.pos.y - this.sprite.position.y,
          );
          const p2 = new Vector3(
            parent.pos.x - this.sprite.position.x,
            parent.pos.y - this.sprite.position.y,
          );
          const line = new Line3(p1, p2);
          bone._line = line;
          for (const prop in bone._opts) {
            if (prop != "weld") {
              bone._opts[prop] = parseFloat(bone._opts[prop]);
            }
          }

          bone._opts.size = (bone._opts.size || 30) / 1024 / 2;
          bone._opts.feather = bone._opts.feather || 1.0;
          bone._opts.blending = bone._opts.blending || 0.7;
        }
      }

      let bonePos = this.sprite.skeleton.bones.map(
        (b) =>
          new Vector2(
            b.pos.x - this.sprite.position.x + 0.0,
            // b.pos.y - this.sprite.position.y - 0.09,
            b.pos.y - this.sprite.position.y - 0.0,
          ),
      );

      bonePos = bonePos.map((b, i) => {
        if (i === 0) {
          return b.clone();
        }
        return b.clone(); //.add(bonePos[i - 1]).multiplyScalar(0.5);//.add(geometry._center);
      });

      // console.log('---- bones');
      // for (const b of bonePos) {
      //   console.log('b', b.x, b.y);
      // }

      geometry.computeBoundingBox();
      geometry.boundingBox.getSize(vertex);
      const hypo = Math.sqrt(vertex.x ** 2 + vertex.y ** 2);
      const colors = new Float32Array(position.count * 3);

      // console.log('---- vertices');
      for (let i = 0; i < position.count; i += 1) {
        vertex.fromBufferAttribute(position, i);
        // console.log('v', vertex.x, vertex.y);
        const dists = bonePos.map((b, index) => {
          const dv = b.clone().sub(vertex);
          return {
            dist: dv.length(),
            angle: dv.normalize().angle() - Math.PI,
            index: index,
            blocked: 0,
            blocks: [],
          };
        });
        dists.sort(sortby("dist"));

        let weights = Clothy.getWeights(
          hypo,
          dists,
          bonePos,
          vertex,
          this.sprite,
        );

        let check = 0;
        let color = new Color(0x000000);
        for (let x = 0; x < 4; x += 1) {
          if (weights.length > x) {
            // if (weights[x].index === 0) { color.r = weights[x].weight; }
            // if (weights[x].index === 1) { color.g = weights[x].weight; }
            // if (weights[x].index === 2) { color.b = weights[x].weight; }
            if (weights[x].index % 3 === 0) {
              color.r += weights[x].weight;
            }
            if (weights[x].index % 3 === 1) {
              color.g += weights[x].weight;
            }
            if (weights[x].index % 3 === 2) {
              color.b += weights[x].weight;
            }
            // if (color.r || color.g || color.b) {
            //   color.a = 1;
            // } else {
            //   color.a = 0.3;
            // }
            check += weights[x].weight;
            if (weights[x].weight < 0) {
              console.error("weight is negative");
            }
            skinIndices.push(weights[x].index);
            skinWeights.push(weights[x].weight);
          } else {
            skinIndices.push(0);
            skinWeights.push(0);
          }
        }
        // if (check < 0.99 || check > 1.01) {
        //   console.log("TODO: check failed", check, weights);
        // }
        colors[i * 3 + 0] = color.r;
        colors[i * 3 + 1] = color.g;
        colors[i * 3 + 2] = color.b;
      }

      geometry.setAttribute(
        "skinIndex",
        new Uint16BufferAttribute(skinIndices, 4),
      );
      geometry.setAttribute(
        "skinWeight",
        new Float32BufferAttribute(skinWeights, 4),
      );
      geometry.setAttribute("color", new BufferAttribute(colors, 3));
    }
  }

  static normalizeAngle(a) {
    a = a % (Math.PI * 2);
    if (a < 0) {
      a = Math.PI * 2 + a;
    }
    return a;
  }

  static distBetweenAngles(a, b) {
    return Math.abs(
      Clothy.normalizeAngle(a - Math.PI) - Clothy.normalizeAngle(b - Math.PI),
    );
  }

  static getWeights(hypo, dists, bones, vertex, sprite) {
    // get closest weights
    const closest = new Vector3();
    const tangents = [];
    for (const bone of sprite.skeleton.bones) {
      if (bone.parent instanceof Bone) {
        const parent = bone.parent;
        bone._line.closestPointToPoint(vertex, true, closest);
        const distance = vertex.distanceTo(closest);
        const p = bone._line.start.distanceTo(closest) / bone._line.distance();

        // pythagorian p
        const d1 = bone._line.start.distanceTo(vertex);
        const d2 = bone._line.end.distanceTo(vertex);

        // const p = d1 / (d1 + d2);

        const s1 = bone._opts.size;
        const s2 = parent._opts.size;
        const threshold = s1 + (s2 - s1) * p;

        // const start = performance.now();
        const w = 1 - distance / threshold;
        if (p >= 0 && p <= 1 && w >= 0 && w <= 1) {
          tangents.push({
            bone,
            parent,
            // closest: closest.clone(),
            weight: w,
            distance,
            p,
          });
        }
      }
    }

    tangents.sort(sortby("distance"));
    const open = [];
    for (const tangent of tangents) {
      if (
        tangent.bone._opts.order != tangent.parent._opts.order ||
        !open.find(
          (o) => o.bone === tangent.parent || o.parent === tangent.bone,
        )
      ) {
        open.push(tangent);
      }
    }

    let weights = [];
    for (const { bone, parent, p, weight } of open) {
      const feather =
        bone._opts.feather + (parent._opts.feather - bone._opts.feather) * p;
      const blending =
        bone._opts.blending + (parent._opts.blending - bone._opts.blending) * p;
      const fw = weight < feather ? 1 - (feather - weight) / feather : 1;
      if (bone._opts.order === parent._opts.order) {
        const tp = 1 - p;
        const fp = tp < blending ? 1 - (blending - tp) / blending : 1;
        weights.unshift({
          index: sprite.skeleton.bones.indexOf(bone),
          weight: fp * fw,
          order: bone._opts.order,
        });
        weights.unshift({
          index: sprite.skeleton.bones.indexOf(parent),
          weight: (1 - fp) * fw,
          order: parent._opts.order,
        });
      } else {
        weights.push({
          index: sprite.skeleton.bones.indexOf(bone),
          weight: fw,
          order: bone._opts.order,
        });
      }
    }

    // weights.sort(sortby('order'));
    // weights = weights.filter(w => w.weight >= 0).slice(0, 4);
    if (weights.length > 0) {
      let total = 1;
      let remaining = weights.slice();
      while (remaining.length > 0) {
        if (total > 0) {
          let order = Math.max(...remaining.map((w) => w.order));
          let group = remaining.filter((r) => r.order === order);
          let groupTotal = group.reduce((a, b) => a + b.weight, 0);
          if (groupTotal > total) {
            for (const weight of group) {
              weight.weight = (weight.weight / groupTotal) * total;
            }
            total = 0;
          } else {
            total -= groupTotal;
          }
          remaining = remaining.filter((r) => r.order < order);
        } else {
          for (const weight of remaining) {
            weight.weight = 0;
          }
          remaining = [];
        }
      }
      if (total > 0) {
        weights.push({ index: 0, weight: total });
      }
      // const totalWeight = Math.abs(weights.reduce((a, b) => a + b.weight, 0));
      // weights = weights.map(w => ({ index: w.index, weight: w.weight / totalWeight }));
      // if (Math.floor(totalWeight * 1000) / 1000 > 1) {
      //   console.log('weights over 1', JSON.stringify(weights), totalWeight);
      // }
    } else {
      // console.log('no weight');
      weights = [{ index: 0, weight: 1 }];
    }

    return weights;
  }

  update() {}

  destroy() {}
}
