// @ts-nocheck
import {
  BlendFunction,
  BloomEffect,
  EffectPass,
  NormalPass,
  OutlineEffect,
  RenderPass,
  SSAOEffect,
} from "postprocessing";
import {
  Color,
  OrthographicCamera,
  PerspectiveCamera,
  Raycaster,
  Scene,
  Vector3,
  WebGLRenderer,
} from "three";
import Mod from "./base";

import GridMeshBehavior from "../behaviors/three/grid";
import MeshBehavior, { geometries, materials } from "../behaviors/three/mesh";
import TextCanvasBehavior from "../behaviors/three/textcanvas";
import { GameObjectDef } from "../engine/defs";

export interface RenderObjectDef extends GameObjectDef {
  type: "module";
  name: "render";
  width: number;
  height: number;
  camera: "perspective" | "orthographic" | "ui";
  background: string;
  effects: false;
}

export default class Render extends Mod {
  camera: PerspectiveCamera | OrthographicCamera;

  get defaults(): RenderObjectDef {
    return {
      background: "white",
      camera: "perspective",
      effects: true,
    };
  }

  constructor(game, data: RenderObjectDef) {
    super(game, data);

    game.geometry = {};
    game.loaders.material = (data) => {
      game.materials[data.name] =
        materials[data.material || "basic"].bind(game)(data);
    };
    game.loaders.geometry = (data) => {
      let retval = geometries[data.shape].bind(game)(data);
      game.geometry[data.name] = retval;
      return retval;
    };

    this.width = this.data.width || window.innerWidth;
    this.height = this.data.height || window.innerHeight;
    this.renderer = new WebGLRenderer({
      canvas: game.options.canvas,
      // logarithmicDepthBuffer: true,
      antialias: true,
      alpha: true,
    });

    // this.renderer.debug.checkShaderErrors = true;
    this.renderer.setSize(this.width, this.height);
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setClearColor(0x00000, 0.0);
    // this.renderer.shadowMap.enabled = true;
    // this.renderer.shadowMap.type = PCFSoftShadowMap;
    // this.renderer.shadowMap.type = BasicShadowMap;
    // this.renderer.shadowMap.type = PCFShadowMap;
    // this.renderer.shadowMap.type = VSMShadowMap;

    // this.composer = new EffectComposer(this.renderer, {
    //   stencilBuffer: true,
    // });

    this.scene = new Scene();
    if (this.data.background && this.data.background != "transparent") {
      this.scene.background = new Color(this.data.background);
    }

    if (this.data.camera == "ui" || this.data.camera == "orthagonal") {
      let aspect = this.width / this.height;
      let height_ortho = this.height;
      let width_ortho = height_ortho * aspect;
      this.camera = new OrthographicCamera(
        width_ortho / -2,
        width_ortho / 2,
        height_ortho / -2,
        height_ortho / 2,
        0.1,
        1000,
      );
      this.camera._type = this.data.camera;
    } else {
      this.camera = new PerspectiveCamera(
        50,
        this.width / this.height,
        0.1,
        1000,
      );
    }

    this.camera.position.z = 30;
    this.camera.up.set(0, 1, 0);
    this.camera.lookAt(new Vector3(0, 0, 0));

    game.width = this.width;
    game.height = this.height;
    game.scene = this.scene;
    game.camera = this.camera;
    game.renderer = this;
    game.renderers.push(this);

    if (this.data.effects) {
      // register effect composer
      const normalPass = new NormalPass(this.scene, this.camera, {
        resolutionScale: 1.0,
      });
      const ssaoEffect = new SSAOEffect(
        this.camera,
        normalPass.renderTarget.texture,
        {
          blendFunction: BlendFunction.SCREEN,
          samples: 11,
          rings: 4,
          distanceThreshold: 0.6,
          distanceFalloff: 0.1,
          rangeThreshold: 0.0015,
          rangeFalloff: 0.01,
          luminanceInfluence: 0.9,
          radius: 55.0,
          scale: 0.3, // 1.0,
          bias: -1.0,
        },
      );

      ssaoEffect.blendMode.opacity.value = 0.15;

      const outlineEffect = new OutlineEffect(this.scene, this.camera, {
        blendFunction: BlendFunction.SCREEN,
        edgeStrength: 5,
        pulseSpeed: 0.0,
        visibleEdgeColor: 0xdddddd,
        hiddenEdgeColor: 0xaaaaaa,
        blur: 40,
        xRay: true,
      });
      const effectPass = new EffectPass(
        this.camera,
        ssaoEffect,
        new BloomEffect({}),
        outlineEffect,
      );
      const renderPass = new RenderPass(this.scene, this.camera);
      renderPass.renderToScreen = false;
      effectPass.renderToScreen = true;
      // renderPass.renderToScreen = true;

      this.composer.addPass(renderPass);
      this.composer.addPass(normalPass);
      this.composer.addPass(effectPass);

      this.outlineEffect = outlineEffect;
    } else {
      // const renderPass = new RenderPass(this.scene, this.camera);
      // renderPass.renderToScreen = true;
      // this.composer.addPass(renderPass);
    }
  }

  draw() {
    for (let sprite of this.game.sprites) {
      sprite.draw();
    }
    // this.composer.render();
    this.renderer.render(this.scene, this.camera);
  }

  update() {
    return false;
  }

  destroy() {
    // TODO:
  }

  outline(mesh) {
    if (mesh) {
      mesh = [mesh];
    } else {
      mesh = [];
    }
    this.outlineEffect.setSelection(mesh);
  }

  unproject(x, y, z?: number, camera?: PerspectiveCamera | OrthographicCamera) {
    if (!camera) {
      camera = this.game.camera;
      camera.updateMatrixWorld();
    }
    let point = new Vector3(
      (x / this.game.width) * 2 - 1,
      -(y / this.game.height) * 2 + 1,
      5,
    ).unproject(camera);
    if (this.camera._type === "ui") {
      // point = camera.position.clone().add(point);
      point.z = 0;
    } else {
      point.sub(camera.position).normalize();
      let distance = -camera.position.z / point.z;
      if (z !== undefined) {
        distance = (z - camera.position.z) / point.z;
      }
      point = camera.position.clone().add(point.multiplyScalar(distance));
    }
    return { x: point.x, y: point.y };
  }

  pick(mouse: { x: number; y: number }) {
    let position = new Vector3();
    position.x = (mouse.x / this.width) * 2 - 1;
    position.y = (-mouse.y / this.height) * 2 + 1; // note we flip Y

    this.raycaster = new Raycaster();

    // cast a ray through the frustum
    this.raycaster.setFromCamera(position, this.camera);
    // get the list of objects the ray intersected
    const intersectedObjects = this.raycaster.intersectObjects(
      this.scene.children,
      true,
    );
    if (intersectedObjects.length) {
      return intersectedObjects.map((o) => {
        o.object._intersect = o.point;
        return o.object;
      });
    }
    return [];
  }
}

// register classes
Render.registerModule("render");
Mod.registerBehavior("three/light", import("../behaviors/three/light.js"));
Mod.registerBehavior("three/ambient", import("../behaviors/three/ambient.js"));
Mod.registerBehavior("three/mesh", MeshBehavior);
Mod.registerBehavior("three/grid", GridMeshBehavior);
Mod.registerBehavior(
  "three/skeleton",
  import("../behaviors/three/skeleton.js"),
);
Mod.registerBehavior("three/clothy", import("../behaviors/three/clothy.js"));
Mod.registerBehavior(
  "three/directional",
  import("../behaviors/three/directional.js"),
);
Mod.registerBehavior("three/textcanvas", TextCanvasBehavior);
