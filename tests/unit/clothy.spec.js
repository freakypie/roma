import Clothy from '../../behaviors/three/clothy.js';
import {assert} from 'chai';

describe('Clothy', async () => {

  describe('getWeights', async () => {

    function test(dists, expected) {
      const weights = Clothy.getWeights(1, dists.map((d, i) => ({ index: i, dist: d }) ));

      console.log('weights2', weights);

      let i = 0;
      for (const w of expected) {
        const actual = weights.find(w => w.index === i);
        // assert.equal(actual.weight, w);
        i += 1;
      }
    }

    // it('should assign the correct bones', async () => {
    //   test([0.5, 0.5], [0.5, 0.5]);
    //   test([0.0, 1.0], [1.0, 0.0]);
    //   test([1.0, 0.0], [0.0, 1.0]);
    //   test([0.25, 0.75], [0.75, 0.25]);
    // });

    it('should assign the correct bones [3]', async () => {
      // test([0.25, 0.25, 0.75], [0.5, 0.5, 0]);
    });

  });

  describe('normalizeAngle', async () => {
    it('should correct over 2 PI', async () => {
      assert.equal(Clothy.normalizeAngle(Math.PI * 2 + 1), 1);
    });
    it('should correct negative angles', async () => {
      assert.equal(Clothy.normalizeAngle(- 1), Math.PI * 2 - 1);
    });
    it('should correct under - 2 PI', async () => {
      assert.equal(Clothy.normalizeAngle(-Math.PI * 2 - 1), Math.PI * 2 - 1);
    });
  });

  describe('distBetweenAngles', async () => {
    it('should fix over 2 PI', async () => {
      assert.equal(Clothy.distBetweenAngles(Math.PI * 2 - 1, 1), 2);
    });
    it('should normalize angles', async () => {
      assert.equal(Clothy.distBetweenAngles(Math.PI * -2, Math.PI * 2), 0);
    });
  });

});