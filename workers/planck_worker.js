/* globals postMessage */
import planck from "planck-js";
import { Quaternion, Vector3 } from "three";
import { BaseProxy } from "./physics_worker.js";

export class PlanckProxy extends BaseProxy {

  createWorld(gravity) {
    let world = new planck.World(new planck.Vec2(gravity[0], gravity[1]));
    this._contactStart = this.contactStart.bind(this);
    this._contactEnd = this.contactEnd.bind(this);
    world.on("begin-contact", this._contactStart);
    world.on("end-contact", this._contactEnd);
    return world;
  }

  setGravity(grav) {
    // console.log('setting gravity', grav[0], grav[1]);
    this.world.setGravity(new planck.Vec2(grav[0], grav[1]));
    for (let body = this.world.getBodyList(); body; body = body.getNext()) {
      body.setAwake(true);
    }
  }

  step() {
    this.world.step(1 / 30, 8, 20);
  }

  contactStart(contact) {
    for (let body of [contact.getFixtureB().getBody(), contact.getFixtureA().getBody()]) {
      // console.log("begin contact", body.didContact);
      body.contactCount = (body.contacts || 0) + 1;
      body.didContact = true;
    }
  }

  contactEnd(contact) {
    for (let body of [contact.getFixtureB().getBody(), contact.getFixtureA().getBody()]) {
      // console.log("end contact", body.didContact, body.contactCount);
      body.contactCount = Math.max(0, (body.contacts || 0) - 1);
    }
  }

  // add body
  createBodyProperties(shape) {
    if (! this._running) {
      let options = {
        position: planck.Vec2(shape.position[0], shape.position[1]),
        angle: shape.angle,
        linearDamping: shape.linear_damping || shape.damping || 0.1,
        angularDamping: shape.angular_damping || shape.damping || 0.0,
        type: shape.static ? planck.Body.KINEMATIC : planck.Body.DYNAMIC,
        fixedRotation: shape.fixed_rotation ? true : false,
        bullet: shape.bullet
      };

      let body = null;
      if (shape.static) {
        body = this.world.createBody(options);
      } else {
        body = this.world.createDynamicBody(options);
      }
      body.isSensor = shape.isSensor ? true : false;

      options = {
        density: shape.density || 0.1,
        friction: shape.friction || 0.1,
        isSensor: shape.isSensor ? true : false,
      };

      if (shape.group) {
        options.filterGroupIndex = shape.group * -1;
      }
      if (shape.category && typeof shape.category === 'string' || shape.category === 0) {
        options.filterCategoryBits = parseInt(shape.category, 2);
      }
      if (shape.mask && typeof shape.category === 'string' || shape.mask === 0) {
        options.filterMaskBits = parseInt(shape.mask, 2);
      }
      if (shape.gravityScale != undefined) {
        options.gravityScale = shape.gravityScale;
      }

      let fixture = null;
      if (shape.type == "circle") {
        fixture = planck.Circle(shape.radius);
      } else if (shape.type == "polygon") {
        fixture = planck.Polygon(shape.vertices);
      } else {
        let center = planck.Vec2(0, 0);
        if (shape.center) {
          center.x = shape.center.x;
          center.y = shape.center.y;
        }
        fixture = planck.Box(shape.width / 2, shape.height / 2, center, shape.angle);
      }

      body.createFixture(fixture, options);
      return body;
    }
    throw new Exception("not creating while running?");
  }

  createJoint(def) {
    let klass = {
      "weld": planck.WeldJoint,
      "revolute": planck.RevoluteJoint,
      "distance": planck.DistanceJoint,
      "rope": planck.RopeJoint,
      "prismatic": planck.PrismaticJoint,
      "mouse": planck.MouseJoint,
    }[def.type];

    const a = this.bodies[def.a];
    const b = this.bodies[def.b];
    const anchor = planck.Vec2(def.position.x, def.position.y);
    let anchor2 = null;
    if (def.position2) {
      anchor2 = planck.Vec2(def.position2.x, def.position2.y)
      // if (!def.position2) {
      //   def.position2 = def.position;
      // }
    }

    // console.log('joint', def, a._id, b._id);

    let joint = this.world.createJoint(klass(def, a, b, anchor, anchor2));
    joint._id = def.id;
    this.joints[def.id] = joint;
  }

  getInitBodyProperties(body) {
    return {
      mass: body.getMass(),
      inertia: body.m_I,
    }
  }

  updateBodyProperties(body, data) {
    if (data.angle !== undefined) {
      body.setAngle(data.angle);
      body.setAwake(true);
    }
    if (data.x !== undefined && data.y !== undefined) {
      body.setPosition(new planck.Vec2({x: data.x, y: data.y}));
      body.setAwake(true);
    }
  }

  destroyBodyProperties(body) {
    this.world.destroyBody(body);
  }

  destroyJointProperties(joint) {
    this.world.destroyJoint(joint);
  }

  applyForce(body, force) {
    body.applyForceToCenter(planck.Vec2(force.x, force.y), true);
  }

  applyTorque(body, torque) {
    // body.applyTorque(data.torque, true);
    body.applyAngularImpulse(torque, true);
    // body.torque += data.torque;
    // console.log("impulse", data.torque);
  }

  changeBodyProperties(body, data) {
    if (data.shape) {
      if (data.shape.group) {
        let fixture = body.getFixtureList();
        let mask = fixture.getFilterMaskBits();
        let options = {};
        if (data.shape.group) {
          options.groupIndex = data.shape.group * -1;
        }
        if (data.shape.category) {
          options.categoryBits = parseInt(data.shape.category, 2);
        }
        if (data.shape.mask) {
          options.maskBits = parseInt(data.shape.mask, 2);
        }
        fixture.setFilterData(options);
      }

      if (data.shape.static) {
        body.setStatic();
      } else if (data.shape.static === false) {
        body.setDynamic();
      }
    }
  }

  changeJointProperties(joint, data) {
    if (data.target) {
      joint.setTarget(planck.Vec2(data.target.x, data.target.y))
    }
  }

  getBodies() {
    let bodies = [];
    for (let body = this.world.getBodyList(); body; body = body.getNext()) {
      if (body.isActive() && (body.isSensor || body.getType() !== "static")) {
        bodies.push(body);
      }
    }
    return bodies;
  }

  getJoints() {
    let joints = [];
    for (let joint = this.world.getJointList(); joint; joint = joint.getNext()) {
      joints.push(joint);
    }
    return joints;
  }

  getBodyProperties(body) {
    let pos = body.getPosition();
    let pv = body.getLinearVelocity();

    let o = new Quaternion();
    o.setFromAxisAngle(new Vector3(0, 0, 1), body.getAngle());
    return {
      r: {x: o.x, y: o.y, z: o.z, w: o.w},
      rv: {x: 0, y: 0, z: body.getAngularVelocity()},
      p: {x: pos.x, y: pos.y, z: 0},
      pv: {x: pv.x, y: pv.y, z: 0},
      contacts: body.contacts,
    }
  }

  getJointProperties(joint) {
    let a = joint.getAnchorA();
    let b = joint.getAnchorB();
    return {
      ax: a.x,
      ay: a.y,
      bx: b.x,
      by: b.y,
      angle: 0, //joint.getReferenceAngle(),
      impulse: 0,  // TODO: impulse
    };
  }
}

let proxy = new PlanckProxy();

onmessage = function (e) {
  e = e.data;
  proxy[e.event](e.data);
};
